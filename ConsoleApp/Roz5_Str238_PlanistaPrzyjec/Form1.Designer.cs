﻿namespace Roz5_Str238_PlanistaPrzyjec
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.fancyOption = new System.Windows.Forms.CheckBox();
            this.healthyOption = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.personNumeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.personNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // fancyOption
            // 
            this.fancyOption.AutoSize = true;
            this.fancyOption.Checked = true;
            this.fancyOption.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fancyOption.Location = new System.Drawing.Point(12, 68);
            this.fancyOption.Name = "fancyOption";
            this.fancyOption.Size = new System.Drawing.Size(163, 21);
            this.fancyOption.TabIndex = 0;
            this.fancyOption.Text = "Dekoracje fantazyjne";
            this.fancyOption.UseVisualStyleBackColor = true;
            this.fancyOption.CheckedChanged += new System.EventHandler(this.fancyOption_CheckedChanged);
            // 
            // healthyOption
            // 
            this.healthyOption.AutoSize = true;
            this.healthyOption.Location = new System.Drawing.Point(12, 95);
            this.healthyOption.Name = "healthyOption";
            this.healthyOption.Size = new System.Drawing.Size(116, 21);
            this.healthyOption.TabIndex = 1;
            this.healthyOption.Text = "Opcja zdrowa";
            this.healthyOption.UseVisualStyleBackColor = true;
            this.healthyOption.CheckedChanged += new System.EventHandler(this.healthyOption_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Koszt: ";
            // 
            // personNumeric
            // 
            this.personNumeric.Location = new System.Drawing.Point(12, 29);
            this.personNumeric.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.personNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.personNumeric.Name = "personNumeric";
            this.personNumeric.Size = new System.Drawing.Size(174, 22);
            this.personNumeric.TabIndex = 3;
            this.personNumeric.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.personNumeric.ValueChanged += new System.EventHandler(this.personNumeric_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ilośc osób: ";
            // 
            // resultLabel
            // 
            this.resultLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.resultLabel.Location = new System.Drawing.Point(86, 129);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(100, 23);
            this.resultLabel.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(198, 168);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.personNumeric);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.healthyOption);
            this.Controls.Add(this.fancyOption);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Planista";
            ((System.ComponentModel.ISupportInitialize)(this.personNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox fancyOption;
        private System.Windows.Forms.CheckBox healthyOption;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown personNumeric;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label resultLabel;
    }
}

