﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DummyFileGenerator
{
    public partial class Form1 : Form
    {
        Random random;
        StringBuilder myStringBuilder;
        private string message;
        private int finalSize;
        public Form1()
        {
            InitializeComponent();
            random = new Random();
            myStringBuilder = new StringBuilder
            {
                Capacity = 1048576
            };
        }
        // Zapis do pliku
        public void MakeFile(int size)
        {
            string filepath = @"C:\Users\mat3u\Documents\xopero\generatedFiles\"
                + "File_"
                + DateTime.Now.Date.ToShortDateString().Replace('/', '_')
                + ".txt";
            finalSize = (int)Math.BigMul(size, 1024);
            for (int i = 0; i <= finalSize; i++)
            {
             //   myStringBuilder.Append(random.Next(1, 2).ToString()).ToString();
                message += random.Next(1, 2).ToString();
            }
            if (!File.Exists(filepath))
            {
                // Metoda zapisu #1
                File.AppendAllText(filepath, message);
                // Metoda zapisu #2
                //using (StreamWriter sw = File.CreateText(filepath))
                //{
                //    sw.WriteLine(myStringBuilder);
                //}
            }
            else
            {
                // Metoda zapisu #1
                File.AppendAllText(filepath, message);
                // Metoda zapisu #2
                //using (StreamWriter sw = File.CreateText(filepath))
                //{
                //    sw.WriteLine(message);
                //}
            }
        }
        // Obsługa formularza
        // Przycisk do tworzenia plików
        private void CreateButton_Click(object sender, EventArgs e)
        {
            MakeFile((int)numericUpDown1.Value);
        }
    }
}
