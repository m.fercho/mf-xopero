﻿namespace Roz4_Str191_KalkulatorOdleglosci
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.startingNum = new System.Windows.Forms.NumericUpDown();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.endingNum = new System.Windows.Forms.NumericUpDown();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lblWynik = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblPokaz = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.startingNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endingNum)).BeginInit();
            this.SuspendLayout();
            // 
            // startingNum
            // 
            this.startingNum.Location = new System.Drawing.Point(234, 17);
            this.startingNum.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.startingNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.startingNum.Name = "startingNum";
            this.startingNum.Size = new System.Drawing.Size(228, 22);
            this.startingNum.TabIndex = 0;
            this.startingNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbl1
            // 
            this.lbl1.Location = new System.Drawing.Point(12, 8);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(204, 39);
            this.lbl1.TabIndex = 2;
            this.lbl1.Text = "Początkowy stan licznika";
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl2
            // 
            this.lbl2.Location = new System.Drawing.Point(12, 59);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(204, 39);
            this.lbl2.TabIndex = 3;
            this.lbl2.Text = "Końcowy stan licznika";
            this.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // endingNum
            // 
            this.endingNum.Location = new System.Drawing.Point(234, 68);
            this.endingNum.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.endingNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.endingNum.Name = "endingNum";
            this.endingNum.Size = new System.Drawing.Size(228, 22);
            this.endingNum.TabIndex = 4;
            this.endingNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbl3
            // 
            this.lbl3.Location = new System.Drawing.Point(12, 108);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(204, 39);
            this.lbl3.TabIndex = 6;
            this.lbl3.Text = "Kwota do zwrotu";
            this.lbl3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWynik
            // 
            this.lblWynik.Location = new System.Drawing.Point(231, 108);
            this.lblWynik.Name = "lblWynik";
            this.lblWynik.Size = new System.Drawing.Size(204, 39);
            this.lblWynik.TabIndex = 7;
            this.lblWynik.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(87, 238);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(283, 51);
            this.button1.TabIndex = 8;
            this.button1.Text = "Oblicz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblPokaz
            // 
            this.lblPokaz.Location = new System.Drawing.Point(231, 158);
            this.lblPokaz.Name = "lblPokaz";
            this.lblPokaz.Size = new System.Drawing.Size(204, 39);
            this.lblPokaz.TabIndex = 11;
            this.lblPokaz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl4
            // 
            this.lbl4.Location = new System.Drawing.Point(12, 158);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(204, 39);
            this.lbl4.TabIndex = 10;
            this.lbl4.Text = "Odległość";
            this.lbl4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 305);
            this.Controls.Add(this.lblPokaz);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblWynik);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.endingNum);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.startingNum);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Kalkulator odległości";
            ((System.ComponentModel.ISupportInitialize)(this.startingNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endingNum)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown startingNum;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.NumericUpDown endingNum;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lblWynik;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblPokaz;
        private System.Windows.Forms.Label lbl4;
    }
}

