﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz4_Str191_KalkulatorOdleglosci
{
    public partial class Form1 : Form
    {
        int startingMileage;
        int endingMileage;
        double milesTraveled;
        double reimburseRate = 0.39;
        double amountOwed;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            startingMileage = (int)startingNum.Value;
            endingMileage = (int)endingNum.Value;
            if (startingMileage < endingMileage)
            {
                milesTraveled = endingMileage - startingMileage;
                amountOwed = milesTraveled * reimburseRate;
                lblWynik.Text = amountOwed.ToString() + " zł";
            }
            else
            {
                MessageBox.Show("Początkowy stan licznika musi byc mniejszy niz końcowy.", 
                "Nie moge obliczyc odległości");
            }
            lblPokaz.Text = milesTraveled + " kilometrów";
        }
        
    }
}
