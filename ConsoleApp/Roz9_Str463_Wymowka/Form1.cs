﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz9_Str463_Wymowka
{
    public partial class Form1 : Form
    {
        private Excuse currentExcuse = new Excuse();
        private string selectedFolder = "";
        private bool formChanged = false;
        Random myRandom = new Random();
        public Form1()
        {
            InitializeComponent();
            currentExcuse.LastUsed = lastUsed.Value;
        } 
        private bool CheckChanged()
        {
            if (formChanged)
            {
                DialogResult result = MessageBox.Show(
                      "Bieżąca wymówka nie została zapisana. Czy kontynuować?",
                       "Ostrzeżenie", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.No)
                    return false;
            }
            return true;
        }
        // Aktualizacja wyświetlania treści w formularzu
        private void UpdateForm(bool changed)
        {
            if (!changed)
            {
                description.Text = currentExcuse.Description;
                result.Text = currentExcuse.Results;
                lastUsed.Value = currentExcuse.LastUsed;
                if (!String.IsNullOrEmpty(currentExcuse.ExcusePath))
                    dateFile.Text = File.GetLastWriteTime(currentExcuse.ExcusePath).ToString();
                Text = "Program do zarządzania wymówkami";
            }
            else
                Text = "Program do zarządzania wymówkami (zmiany nie zostały zapisane)";
            formChanged = changed;
        }
        // Przycisk do wyboru folderu 
        private void buttonFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = selectedFolder;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                selectedFolder = folderBrowserDialog1.SelectedPath;
                buttonSave.Enabled = true;
                buttonOpen.Enabled = true;
                buttonRandom.Enabled = true;
            }
        }
        // Przycisk do zapisu 
        private void buttonSave_Click(object sender, EventArgs e)
        {
            // Zabezpieczenie w przypadku braku danych
            if (String.IsNullOrEmpty(description.Text) || String.IsNullOrEmpty(result.Text))
            {
                MessageBox.Show("Podaj wymówkę i rezultat", "Nie można zapisać pliku", 
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            saveFileDialog1.InitialDirectory = selectedFolder;
            saveFileDialog1.Filter = "Pliki tekstowe (*.txt)|*.txt|Wszystkie pliki (*.*)|*.*";
            saveFileDialog1.FileName = description.Text + ".txt";
            // Komunikat po udanym zapisie
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                currentExcuse.Save(saveFileDialog1.FileName);
                UpdateForm(false);
                MessageBox.Show("Wymówka została zapisana", "Zapisano");
            }
        }
        // Przycisk do otwierania 
        private void buttonOpen_Click(object sender, EventArgs e)
        {
            if (CheckChanged())
            {
                openFileDialog1.InitialDirectory = selectedFolder;
                openFileDialog1.Filter = "Pliki tekstowe (*.txt)|*.txt|Wszystkie pliki (*.*)|*.*";
                openFileDialog1.FileName = description.Text + ".txt";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    currentExcuse = new Excuse(openFileDialog1.FileName);
                    UpdateForm(false);
                }
            }
        }
        // Przycisk do wybierania losowej wymówki 
        private void buttonRandom_Click(object sender, EventArgs e)
        {
            if (CheckChanged())
            {
                currentExcuse = new Excuse(myRandom, selectedFolder);
                UpdateForm(false);
            }
        }
        // Procedury obsługi zdarzeń "changed" dla pól do wpisywania danych
        private void description_TextChanged(object sender, EventArgs e)
        {
            currentExcuse.Description = description.Text;
            UpdateForm(true);
        }

        private void result_TextChanged(object sender, EventArgs e)
        {
            currentExcuse.Results = result.Text;
            UpdateForm(true);
        }

        private void lastUsed_ValueChanged(object sender, EventArgs e)
        {
            currentExcuse.LastUsed = lastUsed.Value;
            UpdateForm(true);
        }
        

        // C:\Users\mat3u\Documents\xopero\ConsoleApp\Roz9_Str463_Wymowka\
    }
}
