﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitDLL
{
    class ByteToReadableValue
    {
        private long temp { get; set; }
        // Metoda zamieniająca Bajty na inne wartości
        public string BytesToReadableValue(long number)
        {
            List<string> suffixes = new List<string> { " B", " KB", " MB", " GB", " TB", " PB" };
            for (int i = 0; i < suffixes.Count; i++)
            {
                temp = number / (int)Math.Pow(1024, i + 1);
                if (temp == 0)
                {
                    return (number / (int)Math.Pow(1024, i)) + suffixes[i];
                }
            }
            return number.ToString();
        }
    }
}
