﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MonitDLL
{
    class Memory
    {
        // Licznik procentowego użycia pamięci (% Committed Bytes In Use) RAM
        PerformanceCounter pRAMPercentCommitted;
        // Licznik dostepnej pamięci (Available MBytes) RAM
        PerformanceCounter pRAMAvailable;
        // Licznik Committed Bytes pamięci RAM
        PerformanceCounter pRAMCommittedBytes;
        // Licznik Commit Limit pamięci RAM
        PerformanceCounter pRAMCommitLimit;
        // Licznik Pool Paged Bytes pamięci RAM
        PerformanceCounter pRAMPoolPagedBytes;
        // Licznik Pool Nonpaged Bytes pamięci RAM
        PerformanceCounter pRAMPoolNonpagedBytes;
        // Licznik Cache Bytes pamięci RAM
        PerformanceCounter pRAMCacheBytes;
        // Instancja klasy do łączenia się z bazą danych
        DBOperations dbOperations;
        private string sqlQuerryInsert, percentRamUsageSqlQuerry, availableRamSqlQuerry, committedBytesSqlQuerry, commitLimitSqlQuerry, poolPagedBytesSqlQuerry, poolNonpagedBytesSqlQuerry, cacheBytesSqlQuerry;
        public Memory()
        {
            pRAMPercentCommitted = new PerformanceCounter("Memory", 
                "% Committed Bytes In Use");
            pRAMAvailable = new PerformanceCounter("Memory", "Available MBytes");
            pRAMCommittedBytes = new PerformanceCounter("Memory", "Committed Bytes");
            pRAMCommitLimit = new PerformanceCounter("Memory", "Commit Limit");
            pRAMPoolPagedBytes = new PerformanceCounter("Memory", "Pool Paged Bytes");
            pRAMPoolNonpagedBytes = new PerformanceCounter("Memory", 
                "Pool Nonpaged Bytes");
            pRAMCacheBytes = new PerformanceCounter("Memory", "Cache Bytes");
            dbOperations = 
                new DBOperations(@"C:\Users\mat3u\Documents\xopero\monitor_DB\monDB.db");
        }
        // Metoda wywoływana w klasie Monitor
        public void MemoryValue(int id)
        {
            percentRamUsageSqlQuerry = Convert.ToInt64(pRAMPercentCommitted.NextValue()).ToString();
            availableRamSqlQuerry = Convert.ToInt32(pRAMAvailable.NextValue()).ToString();
            committedBytesSqlQuerry = Convert.ToInt64(pRAMCommittedBytes.NextValue()).ToString();
            commitLimitSqlQuerry = Convert.ToInt64(pRAMCommitLimit.NextValue()).ToString();
            poolPagedBytesSqlQuerry = Convert.ToInt64(pRAMPoolPagedBytes.NextValue()).ToString();
            poolNonpagedBytesSqlQuerry = Convert.ToInt64(pRAMPoolNonpagedBytes.NextValue()).ToString();
            cacheBytesSqlQuerry = Convert.ToInt64(pRAMCacheBytes.NextValue()).ToString();
            DBInsertMemory(id);
        }
        // Metoda wykonująca zapytanie do bazy danych
        private void DBInsertMemory(int id)
        {
            sqlQuerryInsert = "INSERT INTO ramTable "
                + "(RaportID, PercentUsage, AvailableMemory, " 
                + "CommittedBytes, CommitLimit, " 
                + "PoolPagedBytes, PoolNonpagedBytes, CacheBytes) "
                + "VALUES ('" // ID do raportu: 
                + id
                + "','" // Procentowe zużycie pamięci RAM:      
                + percentRamUsageSqlQuerry
                + "','" // Dostępna pamięć (Available MBytes):         
                + availableRamSqlQuerry
                + "','" // Committed Bytes:              
                + committedBytesSqlQuerry
                + "','" // Commit Limit:           
                + commitLimitSqlQuerry
                + "','" // Pool Paged Bytes:                
                + poolPagedBytesSqlQuerry
                + "','" // Pool Nonpaged Bytes:               
                + poolNonpagedBytesSqlQuerry
                + "','" // Cache Bytes: 
                + cacheBytesSqlQuerry
                + "')";
            dbOperations.DBQuerry(sqlQuerryInsert);
        }
    }
}
