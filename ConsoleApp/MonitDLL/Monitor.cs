﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace MonitDLL
{
    public class Monitor
    {
        CPU cpu;
        Memory memory;
        Processes processes;
        DBOperations dbOperations;
        Timer MonitorTimer;
        private int HowOften;
        public Monitor(int howOften)
        {
            cpu = new CPU();
            memory = new Memory();
            processes = new Processes();
            dbOperations = 
                new DBOperations(@"C:\Users\mat3u\Documents\xopero\monitor_DB\monDB.db");
            HowOften = 60000 * howOften;
            MonitorTimer = new Timer(HowOften);
            MonitorTimer.Elapsed += MonitorTimer_Tick;
        }
        public Monitor()
        {
            dbOperations =
                new DBOperations(@"C:\Users\mat3u\Documents\xopero\monitor_DB\monDB.db");
        }
        // Metoda wywołująca wszystkie metody
        private void AllMeth()
        {
            int id = dbOperations.MakeRaport();
            cpu.CPUValue(id);
            memory.MemoryValue(id);
            processes.ProcessValue(id);
        }
        // "Tiki" zegara
        private void MonitorTimer_Tick(object sender, ElapsedEventArgs e)
        {
            AllMeth();
        }
        // Metoda do uruchamiania monitora zasobów/procesów
        public void MonitorStart()
        {
            MonitorTimer.Start();
        }
        // Metoda do zatrzymywania monitora zasobów/procesów
        public void MonitorStop()
        {
            MonitorTimer.Stop();
        }
        // Metoda do testów monitora zasobów/procesów
        public void MonitorTest()
        {

        }
        // Metoda do jednokrtonego wywołania innych metod - testowanie
        public void MonitorOneTimeRund()
        {
            AllMeth();
        }
        // metoda do przekazywania danych
        public void TransferThing(int howOften)
        {
            HowOften = 60000 * howOften;
        }
    }
}
