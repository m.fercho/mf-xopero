﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitDLL
{
    class DBOperations
    {
        SQLiteConnection dbConnect;
        private string DbPath { get; set; }
        private string sqlQuerryInsert, sqlQuerrySelect;
        public DBOperations(string dbPath)
        {
            DbPath = dbPath;
        }
        public void DBQuerry(string sqlQuerry)
        {
            dbConnect = new SQLiteConnection("Data Source=" + DbPath + ";Version=3;");
            SQLiteCommand command = new SQLiteCommand(sqlQuerry, dbConnect);
            dbConnect.Open();
            command.ExecuteNonQuery();
            dbConnect.Close();
        }
        public int MakeRaport()
        {
            int id = 0;
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string time = DateTime.Now.ToString("HH:mm:ss");
            sqlQuerryInsert = "INSERT INTO raportTable "
                + "(Date, Time) "
                + "VALUES ('" // Data:
                + date
                + "','" // Czas: 
                + time
                + "')";
            DBQuerry(sqlQuerryInsert);
            sqlQuerrySelect = "SELECT ID "
                + "FROM raportTable "
                + "WHERE Date = '"
                + date
                + "' AND Time = '" // Czas: 
                + time
                + "'";
            SQLiteCommand command = new SQLiteCommand(sqlQuerrySelect, dbConnect);
            dbConnect.Open();
            SQLiteDataReader dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                id = Convert.ToInt32(dataReader["ID"]);
            }
            dbConnect.Close();
            return id;
        }
    }
    // Ściezka do pliku bazy danych: 
    // Baza uszkodzona
    // @"C:\Users\mat3u\Documents\xopero\monitor_DB\monitDB.db"
    // Druga baza danych: 
    // @"C:\Users\mat3u\Documents\xopero\monitor_DB\monDB.db"
}
