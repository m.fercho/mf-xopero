﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MonitDLL
{
    class CPU
    {
        // Licznik procesora Processor Time (% zużycia):
        PerformanceCounter pCPUPercentProcessorTime;
        // Licznik procesora % Privileged Time:
        PerformanceCounter pCPUPercentPrivilegedTime;
        // Licznik procesora % Interrupt Time:
        PerformanceCounter pCPUPercentInterruptTime;
        // Licznik procesora % DPC Time:
        PerformanceCounter pCPUPercentDPCTime;
        // Instancja klasy do łączenia się z bazą danych
        DBOperations dbOperations;
        private string sqlQuerryInsert, percentCpuUsageSqlQuerry, percentPrivilegedTimeSqlQuerry, percentInterruptTimeSqlQuerry, percentDPCTimeSqlQuerry;
        public CPU()
        {
            pCPUPercentProcessorTime = new PerformanceCounter("Processor", 
                "% Processor Time", "_Total");
            pCPUPercentPrivilegedTime = new PerformanceCounter("Processor", 
                "% Privileged Time", "_Total");
            pCPUPercentInterruptTime = new PerformanceCounter("Processor", 
                "% Interrupt Time", "_Total");
            pCPUPercentDPCTime = new PerformanceCounter("Processor", 
                "% DPC Time", "_Total");
            pCPUPercentProcessorTime.NextValue();
            pCPUPercentPrivilegedTime.NextValue();
            pCPUPercentInterruptTime.NextValue();
            pCPUPercentDPCTime.NextValue();
            dbOperations = 
                new DBOperations(@"C:\Users\mat3u\Documents\xopero\monitor_DB\monDB.db");
        }
        // Metoda wywoływana w klasie Monitor
        public void CPUValue(int id)
        {
            percentCpuUsageSqlQuerry = 
                pCPUPercentProcessorTime.NextValue().ToString();
            percentPrivilegedTimeSqlQuerry = 
                pCPUPercentPrivilegedTime.NextValue().ToString();
            percentInterruptTimeSqlQuerry = 
                pCPUPercentInterruptTime.NextValue().ToString();
            percentDPCTimeSqlQuerry = pCPUPercentDPCTime.NextValue().ToString();
            DBInsertCPU(id);
        }
        // Metoda wykonująca zapytanie do bazy danych
        private void DBInsertCPU(int id)
        {
            sqlQuerryInsert = "INSERT INTO cpuTable "
                + "(RaportID, PercentUsage, PrivilegedTime, InterruptTime, DPCTime) "
                + "VALUES ('" // ID do raportu: 
                + id
                + "','" // Procentowe zużycie procesora:
                + percentCpuUsageSqlQuerry
                + "','" // Privileged Time
                + percentPrivilegedTimeSqlQuerry
                + "','" // Interrupt Time
                + percentInterruptTimeSqlQuerry
                + "','" // DPC Time
                + percentDPCTimeSqlQuerry
                + "')";
            dbOperations.DBQuerry(sqlQuerryInsert);
        }
    }
}
