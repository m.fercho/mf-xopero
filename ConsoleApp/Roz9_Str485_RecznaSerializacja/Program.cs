﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Roz9_Str485_RecznaSerializacja
{
    class Program
    {
        static void Main(string[] args)
        {
            Card threeTrefl = new Card(Suits.Clubs, Values.Three);
            Card sixKier = new Card(Suits.Hearts, Values.Six);
            /*
            Stream output = File.Create("Karta_1.dat");
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(output, threeTrefl);
            output.Close();

            Stream output2 = File.Create("Karta_2.dat");
            BinaryFormatter bf2 = new BinaryFormatter();
            bf2.Serialize(output2, sixKier);
            output.Close();
            */
            using (Stream output = File.Create("karta1.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(output, threeTrefl);
            }

            Card sixOfHearts = new Card(Suits.Hearts, Values.Six);
            using (Stream output = File.Create("karta2.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(output, sixKier);
            }

            Console.WriteLine("______________________ \r\n");
            byte[] firstFile = File.ReadAllBytes("karta1.dat");
            byte[] secondFile = File.ReadAllBytes("karta2.dat");
            for (int i = 0; i < firstFile.Length; i++)
                if (firstFile[i] != secondFile[i])
                    Console.WriteLine("Bajt numer {0}: {1} i {2}",
                        i, firstFile[i], secondFile[i]);
            
            firstFile[347] = (byte)Suits.Spades;
            firstFile[412] = (byte)Values.King;
            File.Delete("karta3.dat");
            File.WriteAllBytes("karta3.dat", firstFile);

            using (Stream input = File.OpenRead("karta3.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                Card kingOfSpades = (Card)bf.Deserialize(input);
                Console.WriteLine("______________________ \r\n" + kingOfSpades);
            }

            Console.ReadKey();
        }
    }
}
