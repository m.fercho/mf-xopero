﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab3.Model
{
    class Invader : Ship
    {
        public static readonly Size InvaderSize = new Size(15, 15);
        public int Score { get; set; }
        public InvaderType InvaderType { get; set; }
        public const double HorizontalPixelsPerMove = 5;
        public const double VerticalPixelsPerMove = 15;
        public override void Move(Direction direction)
        {
            switch (direction)
            {
                case Direction.Left:
                Location = new Point(Location.X - HorizontalPixelsPerMove, Location.Y);
                break;
                case Direction.Right:
                Location = new Point(Location.X + HorizontalPixelsPerMove, Location.Y);
                break;
                case Direction.Down:
                Location = new Point(Location.X, Location.Y + VerticalPixelsPerMove);
                break;
                default: break;
            }
        }
        public Invader(InvaderType invaderType, Point location, int score)
            : base(location, InvaderSize)
        {
            InvaderType = invaderType;
            Score = score;
        }
    }
}
