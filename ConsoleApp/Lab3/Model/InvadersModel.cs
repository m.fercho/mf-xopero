﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab3.Model
{
    class InvadersModel
    {
        public readonly static Size PlayAreaSize = new Size(400, 300);
        public const int MaximumPlayerShots = 3, InitialStarCount = 50;
        private readonly Random _random = new Random();
        public int Score { get; private set; }
        public int Wave { get; private set; }
        public int Lives { get; private set; }
        public bool GameOver { get; private set; }
        public bool PlayerDying { get { return _playerDied.HasValue; } }
        private DateTime? _playerDied = null;
        private Player _player;
        private readonly List<Invader> _invaders = new List<Invader>();
        private readonly List<Shot> _playerShots = new List<Shot>();
        private readonly List<Shot> _invaderShots = new List<Shot>();
        private readonly List<Point> _stars = new List<Point>();
        private Direction _invaderDirection = Direction.Left;
        private bool _justMovedDown = false;
        private DateTime _lastUpdated = DateTime.MinValue;
        private bool Pause { get; set; }
        // Słownik z punktacją za pokonanych wrogoów
        private readonly Dictionary<InvaderType, int> _invaderScores = new Dictionary<InvaderType, int>()
        {
            { InvaderType.Star, 10 },
            { InvaderType.Satellite, 20 },
            { InvaderType.Saucer, 30 },
            { InvaderType.Bug, 40 },
            { InvaderType.Spaceship, 50 },
        };

        public InvadersModel()
        {
            EndGame();
        }
        public void EndGame()
        {
            GameOver = true;
        }
        // Metoda - początek gry
        public void StartGame()
        {
            GameOver = false;
            // Opróźnianie kolekcji statków/strzałów/gwiazd itp.
            foreach (Invader invader in _invaders)
            {
                OnShipChanged(invader, true);
                _invaders.Clear();
            }
            foreach (Shot shot in _playerShots)
            {
                OnShotMoved(shot, true);
                _playerShots.Clear();
            }
            foreach (Shot shot in _invaderShots)
            {
                OnShotMoved(shot, true);
                _invaderShots.Clear();
            }
            foreach (Point star in _stars)
            {
                OnStarChanged(star, true);
                _stars.Clear();
            }
            // Generowanie nowych gwiazd
            for (int i = 0; i < InitialStarCount; i++)
            {
                AddStar();
            }
            _player = new Player();
            OnShipChanged(_player, false);
            Lives = 2;
            Wave = 0;
            Score = 0;
            NextWave();

        }
        // Metoda powodująca wystrzelenie w stronę najeźdzców
        public void FireShot()
        {
            if (GameOver || PlayerDying || _lastUpdated == DateTime.MinValue)
                return;

            if (_playerShots.Count <= MaximumPlayerShots)
            {
                Shot shotFired = new Shot(new Point(_player.Location.X + (_player.Size.Width / 2) - 1, _player.Location.Y),
                    Direction.Up);
                _playerShots.Add(shotFired);
                OnShotMoved(shotFired, false);
            }
        }
        // Metoda przesuwająca statek gracza
        public void MovePlayer(Direction direction)
        {
            if (!_playerDied.HasValue)
            {
                _player.Move(direction);
                OnShipChanged(_player, false);
            }
        }
        // Metoda odpowiadająca za migotanie gwiazdkami
        public void Twinkle()
        {
            if (_random.Next(2) == 0)
                if (_stars.Count < (1.5 * InitialStarCount) - 1)
                    AddStar();
                else
                if (_stars.Count > (0.85 * InitialStarCount) + 1)
                    RemoveStar();
        }
        // Metoda podtrzymująca grę
        public void Update()
        {
            Twinkle();
            if (!Pause)
            {
                if (_invaders.Count == 0)
                {
                    NextWave();
                }
                if (!PlayerDying)
                {
                    MoveInvaders();
                    ReturnFire();
                    CheckForInvaderCollisions();
                    CheckForPlayerCollisions();
                }
            }
        }
        // Metoda odpowiadająca za dodanie nowej/kolejnej fali wrogów
        public void NextWave()
        {
            Wave++;
            _invaders.Clear();

            InvaderType invaderType;
            for (int row = 0; row < 6; row++)
            {
                switch (row)
                {
                    case 0:
                    invaderType = InvaderType.Spaceship;
                    break;
                    case 1:
                    invaderType = InvaderType.Bug;
                    break;
                    case 2:
                    invaderType = InvaderType.Saucer;
                    break;
                    case 3:
                    invaderType = InvaderType.Satellite;
                    break;
                    default:
                    invaderType = InvaderType.Star;
                    break;
                }
                for (int column = 0; column < 11; column++)
                {
                    Point location = new Point(column * Invader.InvaderSize.Width * 1.4,
                        row * Invader.InvaderSize.Height * 1.4);
                    _invaders.Add(new Invader(invaderType, location, _invaderScores[invaderType]));
                    OnShipChanged(_invaders[_invaders.Count - 1], false);
                }
            }
        }
        // Metoda pozwalająca n dodanie giwazd do kolekcji
        public void AddStar()
        {
            Point newStar = new Point(_random.Next((int)PlayAreaSize.Width - 10), _random.Next((int)PlayAreaSize.Height - 10));
            _stars.Add(newStar);
            OnStarChanged(newStar, false);
        }   
        // Metoda pozwalająca na usuanie gwiazy z kolekcji
        public void RemoveStar()
        {
            Point starToRemove = _stars[_random.Next(_stars.Count)];
            _stars.Remove(starToRemove);
            OnStarChanged(starToRemove, true);
        }
        // Metoda sprawdzjąca czy gracz został trafiony (ew. czy przeciwnik doszedł 
        // do dolnego brzegu ekranu
        private void CheckForPlayerCollisions()
        {
            List<Shot> invaderShots = _invaderShots.ToList();
            
            foreach (Shot shot in invaderShots)
            {
                Rect shotRect = new Rect(shot.Location.X, shot.Location.Y, Shot.ShotSize.Width,
                    Shot.ShotSize.Height);
                if (RectsOverlap(_player.Area, shotRect))
                {
                    if (Lives == 0)
                        EndGame();
                    else
                    {
                        _invaderShots.Remove(shot);
                        OnShotMoved(shot, true);
                        _playerDied = DateTime.Now;
                        OnShipChanged(_player, true);
                        RemoveAllShots();
                        Lives--;
                    }
                }
            }
            // LINQ
            var invadersReachedBottom =
            from invader in _invaders
            where invader.Area.Bottom > _player.Area.Top
            select invader;
            if (invadersReachedBottom.Count() > 0)
                EndGame();
        }
        // Metoda sprawdzjąca czy najeźdźca został trafiony
        private void CheckForInvaderCollisions()
        {
            List<Shot> playerShots = _playerShots.ToList();
            List<Invader> invaders = _invaders.ToList();
            
            foreach (Shot shot in playerShots)
            {
                Rect shotRect = new Rect(shot.Location.X, shot.Location.Y, Shot.ShotSize.Width,
                    Shot.ShotSize.Height);
                // LINQ
                var invadersHit =
                    from invader in invaders
                    where RectsOverlap(invader.Area, shotRect)
                    select invader;

                foreach (Invader deadInvader in invadersHit)
                {
                    _invaders.Remove(deadInvader);
                    OnShipChanged(deadInvader, true);
                    _playerShots.Remove(shot);
                    OnShotMoved(shot, true);
                    Score += deadInvader.Score;
                }
            }
        }
        // Metoda do porusania całą grupą najeźdżców
        private void MoveInvaders()
        {
            TimeSpan timeSinceLastMove = DateTime.Now - _lastUpdated;
            double msBetweenMoves = Math.Max(7 - Wave, 1) * (2 * _invaders.Count());

            if (timeSinceLastMove >= TimeSpan.FromMilliseconds(msBetweenMoves))
            {
                _lastUpdated = DateTime.Now;
                if (_invaderDirection == Direction.Right)
                {
                    var invadersTouchingRightBoundary =
                    from invader in _invaders
                    where invader.Area.Right > PlayAreaSize.Width - Invader.HorizontalPixelsPerMove * 2
                    select invader;
                    if (invadersTouchingRightBoundary.Count() > 0)
                    {
                        _invaderDirection = Direction.Down;
                        foreach (Invader invader in _invaders)
                        {
                            invader.Move(_invaderDirection);
                            OnShipChanged(invader, false);
                        }
                        _justMovedDown = true;
                        _invaderDirection = Direction.Left;
                    }
                    else
                    {
                        _justMovedDown = false;
                        foreach (Invader invader in _invaders)
                        {
                            invader.Move(_invaderDirection);
                            OnShipChanged(invader, false);
                        }
                    }
                }
                else if (_invaderDirection == Direction.Left)
                {
                    var invadersTouchingLeftBoundary =
                        from invader in _invaders
                        where invader.Area.Left < Invader.HorizontalPixelsPerMove * 2
                        select invader;
                    if (invadersTouchingLeftBoundary.Count() > 0)
                    {
                        _invaderDirection = Direction.Down;
                        foreach (Invader invader in _invaders)
                        {
                            invader.Move(_invaderDirection);
                            OnShipChanged(invader, false);
                        }
                        _justMovedDown = true;
                        _invaderDirection = Direction.Right;
                    }
                    else
                    {
                        _justMovedDown = false;
                        foreach (Invader invader in _invaders)
                        {
                            invader.Move(_invaderDirection);
                            OnShipChanged(invader, false);
                        }
                    }
                }
            }
        }
        // Metoda pozwalajaca najeźdźcom dać ognia
        private void ReturnFire()
        {
            if (_invaderShots.Count() > Wave + 1 || _random.Next(10) < 10 - Wave)
                return;
            var invaderColumns =
                from invader in _invaders
                group invader by invader.Location.X
                into invaderGroup
                orderby invaderGroup.Key descending
                select invaderGroup;
            var randomGroup = invaderColumns.ElementAt(_random.Next(invaderColumns.Count()));
            var shooter = randomGroup.Last();
            Point shotLocation = new Point(shooter.Area.X + (shooter.Size.Width / 2) - 1, shooter.Area.Bottom);
            Shot invaderShot = new Shot(shotLocation, Direction.Down);
            _invaderShots.Add(invaderShot);
            OnShotMoved(invaderShot, false);
        }
        // Usuwanie z widoku wszystkich strzałów
        private void RemoveAllShots()
        {
            List<Shot> invaderShots = _invaderShots.ToList();
            List<Shot> playerShots = _playerShots.ToList();

            foreach (Shot shot in invaderShots)
                OnShotMoved(shot, true);

            foreach (Shot shot in playerShots)
                OnShotMoved(shot, true);

            _invaderShots.Clear();
            _playerShots.Clear();
        }
        // Metoda potrzebna do sprawdzania kolizji z rozdziału 16
        private static bool RectsOverlap(Rect r1, Rect r2)
        {
            r1.Intersect(r2);
            if (r1.Width > 0 || r1.Height > 0)
                return true;
            return false;
        }
        // Procedury pbsługi zdarzeń
        public event EventHandler<ShotMovedEventArgs> ShotMoved;
        public event EventHandler<StarChangedEventArgs> StarChanged;
        public event EventHandler<ShipChangedEventArgs> ShipChanged;
        public void OnShotMoved(Shot shot, bool disappeared)
        {
            ShotMoved?.Invoke(this, new ShotMovedEventArgs(shot, disappeared));
        }
        public void OnStarChanged(Point point, bool disappeared)
        {
            StarChanged?.Invoke(this, new StarChangedEventArgs(point, disappeared));
        }
        public void OnShipChanged(Ship shipUpdated, bool killed)
        {
            ShipChanged?.Invoke(this, new ShipChangedEventArgs(shipUpdated, killed));
        }
    }
}
