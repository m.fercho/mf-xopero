﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str312_PlanistaPrzyjecDziedzicz
{
    class Party
    {
        public const int CostOfFoodPerPerson = 25;
        public int NumberOfPeople { get; set; }
        public bool FancyDecorations { get; set; }
        private decimal CalculateCostOfDecorations()
        {
            decimal costOfDecoration;
            if (FancyDecorations)
            {
                costOfDecoration = (NumberOfPeople * 15.00M) + 50M;
            }
            else
            {
                costOfDecoration = (NumberOfPeople * 7.50M) + 30M;
            }
            return costOfDecoration;
        }
        virtual public decimal Cost
        {
            get
            {
                decimal totalCost = CalculateCostOfDecorations();
                totalCost += CostOfFoodPerPerson * NumberOfPeople;
                if (NumberOfPeople > 12)
                    totalCost += 100;
                return totalCost;
            }
        }
    }
}
