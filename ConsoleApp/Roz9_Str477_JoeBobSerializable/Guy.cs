﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Roz9_Str477_JoeBobSerializable
{
    [Serializable]
    class Guy
    {
        public string Name;
        public int Cash;

        public int GiveCash(int amount)
        {
            if (amount <= Cash && amount > 0)
            {
                Cash -= amount;
                return amount;
            }
            else
            {
                MessageBox.Show("Nie mam wystarczającej ilości pieniędzy, żeby Ci dać " + amount + "zł",
                    Name + "powiedział...");
                return 0;
            }
        }
        public int ReceiveCash(int amount)
        {
            if (amount > 0)
            {
                Cash += amount;
                return amount;
            }
            else
            {
                MessageBox.Show(amount + "zł to nie jest ilość pieniędzy jaką mogę Ci dać ",
                Name + "powiedział...");
                return 0;
            }
        }
    }
}
