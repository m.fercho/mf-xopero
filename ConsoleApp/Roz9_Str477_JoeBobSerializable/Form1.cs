﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Roz9_Str477_JoeBobSerializable
{
    public partial class Form1 : Form
    {
        Guy joe;
        Guy bob;
        int bank = 100;

        public Form1()
        {
            InitializeComponent();

            bob = new Guy
            {
                Name = "Bob",
                Cash = 100
            };
            joe = new Guy
            {
                Name = "Joe",
                Cash = 50
            };
            UpdateForm();

        }
        public void UpdateForm()
        {
            joesCashLabel.Text = joe.Name + " posiada: " + joe.Cash;
            bobsCashLabel.Text = bob.Name + " posiada: " + bob.Cash;
            bankCashLabel.Text = "Bank posiada: " + bank;
        }
        // ReceiveCash
        private void button1_Click(object sender, EventArgs e)
        {
            if (bank >= 10)
            {
                bank -= joe.ReceiveCash(10);
                UpdateForm();
            }
            else
            {
                MessageBox.Show("Bank nie posiada wystarczającej ilości pieniędzy");
            }
        }
        // GiveCash
        private void button2_Click(object sender, EventArgs e)
        {
            bank += bob.GiveCash(5);
            UpdateForm();
        }
        // Joe daje 10zł Bobowi
        private void joeGivesToBob_Click(object sender, EventArgs e)
        {
            joe.ReceiveCash(bob.GiveCash(10));
            UpdateForm();
        }
        // Bob daje 5zł Joemu
        private void bobGivesToJoe_Click(object sender, EventArgs e)
        {
            bob.ReceiveCash(joe.GiveCash(5));
            UpdateForm();
        }

        private void saveJoe_Click(object sender, EventArgs e)
        {

            using (Stream output = File.Create("Plik_faceta.dat"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(output, joe);
            }
        }

        private void loadJoe_Click(object sender, EventArgs e)
        {
            using (Stream input = File.OpenRead("Plik_faceta.dat"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                joe = (Guy)formatter.Deserialize(input);
            }
            UpdateForm();
        }
    }
}
