﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz8_Str416_DwieTalie
{
    public partial class Form1 : Form
    {
        Deck deck1;
        Deck deck2;
        Random myRandom = new Random();
        public Form1()
        {
            InitializeComponent();
            ResetDeck(1);
            ResetDeck(2);
            RedrawDeck(1);
            RedrawDeck(2);
        }
        // Metody
        // Reset
        private void ResetDeck(int deckNumber)
        {
            if (deckNumber == 1)
            {
                int numberOfCards = myRandom.Next(1, 11);
                deck1 = new Deck(new Card[] { });
                for (int i = 0; i < numberOfCards; i++)
                    deck1.Add(new Card((Suits)myRandom.Next(4),
                                       (Values)myRandom.Next(1, 14)));
                deck1.Sort();
            }
            else
                deck2 = new Deck();
        }
        // "Rysowanie" kart w listBox
        private void RedrawDeck(int DeckNumber)
        {
            if (DeckNumber == 1)
            {
                list1.Items.Clear();
                foreach (string cardName in deck1.GetCardNames())
                    list1.Items.Add(cardName);
                label1.Text = "Zestaw 1#. (" + deck1.Count + " kart)";
            }
            else
            {
                list2.Items.Clear();
                foreach (string cardName in deck2.GetCardNames())
                    list2.Items.Add(cardName);
                label2.Text = "Zestaw #2. (" + deck2.Count + " kart)";
            }
        }
        // Obsługa przycisków
        // Przesuwanie kart z talii do talii
        private void moveToDeck2_Click(object sender, EventArgs e)
        {
            if (list1.SelectedIndex >= 0)
                if (deck1.Count > 0)
                    deck2.Add(deck1.Deal(list1.SelectedIndex));
            RedrawDeck(1);
            RedrawDeck(2);
        }
        private void moveToDeck1_Click(object sender, EventArgs e)
        {
            if (list2.SelectedIndex >= 0)
                if (deck2.Count > 0)
                {
                    deck1.Add(deck2.Deal(list2.SelectedIndex));
                }
            RedrawDeck(1);
            RedrawDeck(2);
        }
        // Przywróc zestaw 1
        private void reset1_Click(object sender, EventArgs e)
        {
            ResetDeck(1);
            RedrawDeck(1);
        }
        // Przywróc zestaw 2
        private void reset2_Click(object sender, EventArgs e)
        {
            ResetDeck(2);
            RedrawDeck(2);
        }
        // Wymieszaj zestaw 1
        private void shuffle1_Click(object sender, EventArgs e)
        {
            deck1.Shuffle();
            RedrawDeck(1);
        }
        // Wymieszaj zestaw 2
        private void shuffle2_Click(object sender, EventArgs e)
        {
            deck2.Shuffle();
            RedrawDeck(2);
        }
    }
}
