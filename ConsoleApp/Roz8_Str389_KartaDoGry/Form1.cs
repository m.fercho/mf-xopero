﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz8_Str389_KartaDoGry
{
    public partial class Form1 : Form
    {
        Random myRandom = new Random();
        Card myAceOfSpades = new Card(Suits.Spades, Values.Ace);
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cardNameAce = myAceOfSpades.Name;
            MessageBox.Show(cardNameAce);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Card card = new Card((Suits)myRandom.Next(4), (Values)myRandom.Next(1, 14));
            MessageBox.Show(card.Name);
        }
    }
}
