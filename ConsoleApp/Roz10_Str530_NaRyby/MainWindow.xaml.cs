﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Roz10_Str530_NaRyby
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Game game;
        public MainWindow()
        {
            InitializeComponent();
            game = FindResource("game") as Game;
        }

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            game.StartGame();
        }

        private void listHand_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listHand.SelectedIndex > 0)
            {
                game.PlayOneRound(listHand.SelectedIndex);
            }
        }

        private void buttonAsk_Click(object sender, RoutedEventArgs e)
        {
            if (listHand.SelectedIndex > 0)
            {
                game.PlayOneRound(listHand.SelectedIndex);
            }
        }
    }
}
