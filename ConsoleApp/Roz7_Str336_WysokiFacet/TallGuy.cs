﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str336_WysokiFacet
{
    class TallGuy : IClown
    {
        public string Name;
        public int Height;
        public void TalkAboutYourself()
        {
            Console.WriteLine("Moje imie to: " + Name + " i mam " + Height + "cm wzrostu.");
        }
        public string FunnyThingIHave
        {
            get { return "duże buty"; }
        }
        public void Honk()
        {
            Console.WriteLine("Tu tuut");
        }
    }
}
