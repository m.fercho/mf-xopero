﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str336_WysokiFacet
{
    public interface IClown
    {
        string FunnyThingIHave { get; }
        void Honk();
    }
}
