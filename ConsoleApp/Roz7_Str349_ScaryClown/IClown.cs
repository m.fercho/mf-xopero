﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str349_ScaryClown
{
    public interface IClown
    {
        string FunnyThingIHave { get; }
        void Honk();
    }
}
