﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str317_ZarzadzanieUlem
{
    class Worker : Bee
    {
        public const double honeyUnitsPerShiftWorked = .65;
        public Worker(string[] jobsICanDo, double weightMg) : base (weightMg)
        {
            this.jobsICanDo = jobsICanDo;
        }
        public int ShiftsLeft
        {
            get
            {
                return shiftsToWork - shiftsWorked;
            }
        }
        private string currentJobs = "";
        public string CurrentJob
        {
            get
            {
                return currentJobs;
            }
        }
        private string[] jobsICanDo;
        private int shiftsToWork;
        private int shiftsWorked;
        public bool DoThisJob(string job, int numberOfShifts)
        {
            if (!String.IsNullOrEmpty(currentJobs))
            {
                return false;
            }
            for (int i = 0; i < jobsICanDo.Length; i++)
            {
                if (jobsICanDo[i] == job)
                {
                    currentJobs = job;
                    this.shiftsToWork = numberOfShifts;
                    shiftsWorked = 0;
                    return true;
                }
            }
            return false;
        }
        public bool DidYouFinish()
        {
            if (String.IsNullOrEmpty(currentJobs))
            {
                return false;
            }
            shiftsWorked++;
            if (shiftsWorked > shiftsToWork)
            {
                shiftsWorked = 0;
                shiftsToWork = 0;
                currentJobs = "";
                return true;
            }
            else
                return false;
        }
        public override double HoneyConsumptionRate()
        {
            double consumption = base.HoneyConsumptionRate();
            consumption += shiftsWorked * HoneyUnitsConsumedPerMg;
            return consumption;
        }
    }
}
