﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public class Greyhound
    {
        public int StartingPosition; // Początek trasy
        public int RacetrackLenght; // Długość trasy
        public PictureBox MyPictureBox = null; // Mój obiekt pictureBox (chodzi o pieseła)
        public int Location = 0; // Moje położenie na torze wyścigowym
        public Random Randomizer; // Instancja klasy Random
        /**
         * Przesuń się do przodu losowo o 1, 2, 3 lub 4 punkty
         * Zaktualizuj położenie PictureBox na formularzu
         * Zwróć true, jeśli wygrałem wyścig
         **/
        public bool Run()
        {
         //   Randomizer = new Random();
            int distance = Randomizer.Next(10, 25);
            Console.WriteLine(distance);
            bool GoingForward = true;
            if (GoingForward)
            {
                MyPictureBox.Left += distance;
                Location += distance;
                if (Location >= RacetrackLenght - StartingPosition)
                {
                    GoingForward = false;
                    return true;
                }
            }
            return false;
        }
        // Wyzeruj położenie i ustaw na pozycji startowej
        public void TakeStartingPosition()
        {
            MyPictureBox.Left = 21;
            Location = 0;
        }
    }
}
