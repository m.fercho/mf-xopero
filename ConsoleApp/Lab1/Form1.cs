﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form1 : Form
    {
        Greyhound[] dogs = new Greyhound[4];
        Guy[] guys = new Guy[3];
        Random random = new Random();

        public int Winner { get; private set; }

        public Form1()
        {
            InitializeComponent();
            BeginSetup();
        }
        public void BeginSetup()
        {
            minimumBetLabel.Text = "Minimalny zakład " + betAmount.Minimum + "zł";
            nameLabel.Text = "Janek";
            // Deklkaracja danych piesełów
            dogs[0] = new Greyhound
            {
                MyPictureBox = dog0,
                StartingPosition = dog0.Left,
                RacetrackLenght = raceTrackPic.Width - dog0.Width,
                Randomizer = random, 
            };
            dogs[1] = new Greyhound
            {
                MyPictureBox = dog1,
                StartingPosition = dog1.Left,
                RacetrackLenght = raceTrackPic.Width - dog1.Width,
                Randomizer = random,
            };
            dogs[2] = new Greyhound
            {
                MyPictureBox = dog2,
                StartingPosition = dog2.Left,
                RacetrackLenght = raceTrackPic.Width - dog2.Width,
                Randomizer = random,
            };
            dogs[3] = new Greyhound
            {
                MyPictureBox = dog3,
                StartingPosition = dog3.Left,
                RacetrackLenght = raceTrackPic.Width - dog3.Width,
                Randomizer = random,
            };

            // Deklaracja danych facetów
            guys[0] = new Guy("Janek", null, 50, joeRadioButton, joeBetLabel);
            guys[1] = new Guy( "Bartek",  null,  75,  bobRadioButton,  bobBetLabel);
            guys[2] = new Guy("Arek", null, 45, alRadioButton, alBetLabel);
            for (int i = 0; i < 3; i++)
            {
                guys[i].UpdateLabels();
            }
        }

        // Wybór osoby obstawiającej
        // Joe / Janek
        private void joeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (joeRadioButton.Checked)
                nameLabel.Text = guys[0].Name;
        }
        // Bob / Bartek
        private void bobRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (bobRadioButton.Checked)
                nameLabel.Text = guys[1].Name;
        }
        // Al / Arek
        private void alRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (alRadioButton.Checked)
                nameLabel.Text = guys[2].Name;
        }
        // Przycisk 'stawia' do obstawiania zakładów
        private void betButton_Click(object sender, EventArgs e)
        {
            int i = 0;
            if (joeRadioButton.Checked)
            {
                i = 0;
            }
            if (bobRadioButton.Checked)
            {
                i = 1;
            }
            if (alRadioButton.Checked)
            {
                i = 2;
            }
            guys[i].PlaceBet((int)betAmount.Value, (int)dogNumeric.Value);
            guys[i].UpdateLabels();
        }
        // przycisk 'start' do uruchamiania gry po obstawieniu zakładów
        private void startButton_Click(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < dogs.Length; i++)
            {
                if (dogs[i].Run())
                {
                    timer1.Stop();
                    int paramParam = i + 1; // Zmienna pomocnicza do wyświetlania nr psa
                    string message = "Wygrywa pieseł z numerem " + paramParam;
                    string title = "Yupi! Mamy zwycięzcę";
                    MessageBoxButtons boxButton = MessageBoxButtons.OK;
                    DialogResult result = MessageBox.Show(message, title, boxButton);
                    if (result == DialogResult.OK)
                    {
                        foreach (Guy guy in guys)
                        {
                            if (guy.MyBet != null)
                            {
                                guy.Collect(paramParam);
                                guy.MyBet = null;
                                guy.UpdateLabels();
                            }
                        }
                        foreach (Greyhound dog in dogs)
                        {
                            dog.TakeStartingPosition();
                        }
                        groupBox1.Enabled = true;
                    }
                }
            }
        }
    }
}
