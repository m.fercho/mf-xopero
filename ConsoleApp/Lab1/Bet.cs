﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public class Bet
    {
        public int Amount; // Ilośc postawionych pieniędzy
        public string desc; // Pomocnicza zmienna do metody GetDescription()
        public int Dog; // Numer psa, na któego postawiono
        public Guy Bettor; // Facet, który zawarł układ
        public Bet(int Amount, int Dog, Guy Bettor)
        {
            this.Amount = Amount;
            this.Dog = Dog;
            this.Bettor = Bettor;
        }
        /**
         * Zwraca string, który okresla, kto obstawił wyścig, jak duzo postawił
         * i na którgo psa (np. "Janek postawił 8zł na psa numer 1")
         * Jezeli ilośc jest równa zero, zakład nie został zawarty
         * ("Janek nie zawarł zakładu.")
         **/
        public string GetDescription()
        {
            if (Amount > 0)
            {
                desc = (Bettor + " postawił " + Amount + "zł na psa numer " + Dog);
            }
            else
            {
                desc = (Bettor + " nie zawarł zakładu.");
            }
            return desc;
        }
        /**
         * Parametrem jest zwycięzca wyścigu. Jeżeli pies wygrał, 
         * zwróc wartość postawioną, w przeciwnym razie 
         * zwróc wartość postawioną ze znakiem mknus
         **/
        public int PayOut(int Winner)
        {
            if (Dog == Winner)
            {
                return Amount;
            }
            return -Amount;
        }
    }
}
