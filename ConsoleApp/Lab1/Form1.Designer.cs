﻿namespace Lab1
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.raceTrackPic = new System.Windows.Forms.PictureBox();
            this.betButton = new System.Windows.Forms.Button();
            this.joeRadioButton = new System.Windows.Forms.RadioButton();
            this.bobRadioButton = new System.Windows.Forms.RadioButton();
            this.alRadioButton = new System.Windows.Forms.RadioButton();
            this.minimumBetLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.labelBety = new System.Windows.Forms.Label();
            this.betAmount = new System.Windows.Forms.NumericUpDown();
            this.dog0 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.alBetLabel = new System.Windows.Forms.Label();
            this.bobBetLabel = new System.Windows.Forms.Label();
            this.joeBetLabel = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.labelNaCharta = new System.Windows.Forms.Label();
            this.dogNumeric = new System.Windows.Forms.NumericUpDown();
            this.dog1 = new System.Windows.Forms.PictureBox();
            this.dog2 = new System.Windows.Forms.PictureBox();
            this.dog3 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.raceTrackPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.betAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dog0)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dogNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dog1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dog2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dog3)).BeginInit();
            this.SuspendLayout();
            // 
            // raceTrackPic
            // 
            this.raceTrackPic.Image = ((System.Drawing.Image)(resources.GetObject("raceTrackPic.Image")));
            this.raceTrackPic.Location = new System.Drawing.Point(12, 12);
            this.raceTrackPic.Name = "raceTrackPic";
            this.raceTrackPic.Size = new System.Drawing.Size(812, 254);
            this.raceTrackPic.TabIndex = 0;
            this.raceTrackPic.TabStop = false;
            // 
            // betButton
            // 
            this.betButton.Location = new System.Drawing.Point(134, 192);
            this.betButton.Name = "betButton";
            this.betButton.Size = new System.Drawing.Size(139, 44);
            this.betButton.TabIndex = 1;
            this.betButton.Text = "stawia";
            this.betButton.UseVisualStyleBackColor = true;
            this.betButton.Click += new System.EventHandler(this.betButton_Click);
            // 
            // joeRadioButton
            // 
            this.joeRadioButton.AutoSize = true;
            this.joeRadioButton.Checked = true;
            this.joeRadioButton.Location = new System.Drawing.Point(20, 102);
            this.joeRadioButton.Name = "joeRadioButton";
            this.joeRadioButton.Size = new System.Drawing.Size(126, 21);
            this.joeRadioButton.TabIndex = 2;
            this.joeRadioButton.TabStop = true;
            this.joeRadioButton.Text = "joeRadioButton";
            this.joeRadioButton.UseVisualStyleBackColor = true;
            this.joeRadioButton.CheckedChanged += new System.EventHandler(this.joeRadioButton_CheckedChanged);
            // 
            // bobRadioButton
            // 
            this.bobRadioButton.AutoSize = true;
            this.bobRadioButton.Location = new System.Drawing.Point(20, 129);
            this.bobRadioButton.Name = "bobRadioButton";
            this.bobRadioButton.Size = new System.Drawing.Size(131, 21);
            this.bobRadioButton.TabIndex = 3;
            this.bobRadioButton.Text = "bobRadioButton";
            this.bobRadioButton.UseVisualStyleBackColor = true;
            this.bobRadioButton.CheckedChanged += new System.EventHandler(this.bobRadioButton_CheckedChanged);
            // 
            // alRadioButton
            // 
            this.alRadioButton.AutoSize = true;
            this.alRadioButton.Location = new System.Drawing.Point(20, 157);
            this.alRadioButton.Name = "alRadioButton";
            this.alRadioButton.Size = new System.Drawing.Size(118, 21);
            this.alRadioButton.TabIndex = 4;
            this.alRadioButton.Text = "alRadioButton";
            this.alRadioButton.UseVisualStyleBackColor = true;
            this.alRadioButton.CheckedChanged += new System.EventHandler(this.alRadioButton_CheckedChanged);
            // 
            // minimumBetLabel
            // 
            this.minimumBetLabel.AutoSize = true;
            this.minimumBetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.minimumBetLabel.Location = new System.Drawing.Point(15, 49);
            this.minimumBetLabel.Name = "minimumBetLabel";
            this.minimumBetLabel.Size = new System.Drawing.Size(258, 29);
            this.minimumBetLabel.TabIndex = 5;
            this.minimumBetLabel.Text = "Minimalny zakład 5zł";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(17, 205);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(43, 17);
            this.nameLabel.TabIndex = 6;
            this.nameLabel.Text = "name";
            // 
            // labelBety
            // 
            this.labelBety.AutoSize = true;
            this.labelBety.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBety.Location = new System.Drawing.Point(314, 49);
            this.labelBety.Name = "labelBety";
            this.labelBety.Size = new System.Drawing.Size(106, 29);
            this.labelBety.TabIndex = 7;
            this.labelBety.Text = "Zakłady";
            // 
            // betAmount
            // 
            this.betAmount.Location = new System.Drawing.Point(319, 204);
            this.betAmount.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.betAmount.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.betAmount.Name = "betAmount";
            this.betAmount.Size = new System.Drawing.Size(68, 22);
            this.betAmount.TabIndex = 8;
            this.betAmount.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // dog0
            // 
            this.dog0.Image = ((System.Drawing.Image)(resources.GetObject("dog0.Image")));
            this.dog0.Location = new System.Drawing.Point(21, 36);
            this.dog0.Name = "dog0";
            this.dog0.Size = new System.Drawing.Size(81, 22);
            this.dog0.TabIndex = 12;
            this.dog0.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.alBetLabel);
            this.groupBox1.Controls.Add(this.bobBetLabel);
            this.groupBox1.Controls.Add(this.joeBetLabel);
            this.groupBox1.Controls.Add(this.startButton);
            this.groupBox1.Controls.Add(this.labelNaCharta);
            this.groupBox1.Controls.Add(this.dogNumeric);
            this.groupBox1.Controls.Add(this.minimumBetLabel);
            this.groupBox1.Controls.Add(this.betButton);
            this.groupBox1.Controls.Add(this.joeRadioButton);
            this.groupBox1.Controls.Add(this.bobRadioButton);
            this.groupBox1.Controls.Add(this.alRadioButton);
            this.groupBox1.Controls.Add(this.nameLabel);
            this.groupBox1.Controls.Add(this.labelBety);
            this.groupBox1.Controls.Add(this.betAmount);
            this.groupBox1.Location = new System.Drawing.Point(12, 272);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(812, 370);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dom bukmacherski";
            // 
            // alBetLabel
            // 
            this.alBetLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.alBetLabel.Location = new System.Drawing.Point(319, 159);
            this.alBetLabel.Name = "alBetLabel";
            this.alBetLabel.Size = new System.Drawing.Size(275, 19);
            this.alBetLabel.TabIndex = 17;
            this.alBetLabel.Text = "alBetLabel";
            // 
            // bobBetLabel
            // 
            this.bobBetLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bobBetLabel.Location = new System.Drawing.Point(319, 131);
            this.bobBetLabel.Name = "bobBetLabel";
            this.bobBetLabel.Size = new System.Drawing.Size(275, 19);
            this.bobBetLabel.TabIndex = 16;
            this.bobBetLabel.Text = "bobBetLabel";
            // 
            // joeBetLabel
            // 
            this.joeBetLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.joeBetLabel.Location = new System.Drawing.Point(319, 104);
            this.joeBetLabel.Name = "joeBetLabel";
            this.joeBetLabel.Size = new System.Drawing.Size(275, 19);
            this.joeBetLabel.TabIndex = 15;
            this.joeBetLabel.Text = "joeBetLabel";
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.startButton.Location = new System.Drawing.Point(9, 251);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(585, 105);
            this.startButton.TabIndex = 14;
            this.startButton.Text = "START";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // labelNaCharta
            // 
            this.labelNaCharta.AutoSize = true;
            this.labelNaCharta.Location = new System.Drawing.Point(393, 206);
            this.labelNaCharta.Name = "labelNaCharta";
            this.labelNaCharta.Size = new System.Drawing.Size(126, 17);
            this.labelNaCharta.TabIndex = 13;
            this.labelNaCharta.Text = "zł na charta numer";
            // 
            // dogNumeric
            // 
            this.dogNumeric.Location = new System.Drawing.Point(524, 204);
            this.dogNumeric.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.dogNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dogNumeric.Name = "dogNumeric";
            this.dogNumeric.Size = new System.Drawing.Size(70, 22);
            this.dogNumeric.TabIndex = 12;
            this.dogNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dog1
            // 
            this.dog1.Image = ((System.Drawing.Image)(resources.GetObject("dog1.Image")));
            this.dog1.Location = new System.Drawing.Point(21, 84);
            this.dog1.Name = "dog1";
            this.dog1.Size = new System.Drawing.Size(81, 22);
            this.dog1.TabIndex = 17;
            this.dog1.TabStop = false;
            // 
            // dog2
            // 
            this.dog2.Image = ((System.Drawing.Image)(resources.GetObject("dog2.Image")));
            this.dog2.Location = new System.Drawing.Point(21, 139);
            this.dog2.Name = "dog2";
            this.dog2.Size = new System.Drawing.Size(81, 22);
            this.dog2.TabIndex = 18;
            this.dog2.TabStop = false;
            // 
            // dog3
            // 
            this.dog3.Image = ((System.Drawing.Image)(resources.GetObject("dog3.Image")));
            this.dog3.Location = new System.Drawing.Point(21, 215);
            this.dog3.Name = "dog3";
            this.dog3.Size = new System.Drawing.Size(81, 22);
            this.dog3.TabIndex = 19;
            this.dog3.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 658);
            this.Controls.Add(this.dog3);
            this.Controls.Add(this.dog2);
            this.Controls.Add(this.dog1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dog0);
            this.Controls.Add(this.raceTrackPic);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Dzień na wyścigach";
            ((System.ComponentModel.ISupportInitialize)(this.raceTrackPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.betAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dog0)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dogNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dog1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dog2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dog3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox raceTrackPic;
        private System.Windows.Forms.Button betButton;
        private System.Windows.Forms.RadioButton joeRadioButton;
        private System.Windows.Forms.RadioButton bobRadioButton;
        private System.Windows.Forms.RadioButton alRadioButton;
        private System.Windows.Forms.Label minimumBetLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label labelBety;
        private System.Windows.Forms.NumericUpDown betAmount;
        private System.Windows.Forms.PictureBox dog0;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelNaCharta;
        private System.Windows.Forms.NumericUpDown dogNumeric;
        private System.Windows.Forms.PictureBox dog1;
        private System.Windows.Forms.PictureBox dog2;
        private System.Windows.Forms.PictureBox dog3;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label bobBetLabel;
        private System.Windows.Forms.Label joeBetLabel;
        private System.Windows.Forms.Label alBetLabel;
        private System.Windows.Forms.Timer timer1;
    }
}

