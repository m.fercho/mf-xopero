﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public class Guy 
    {
        public string Name; // Imię faceta
        public Bet MyBet; // Instancja klasy Bet przechowująca dane o zakładzie
        public int Cash; // Jak dużo pieniędzy posiada
        public RadioButton MyRadioButton; // Moje pole wyboru
        public Label MyLabel; // Moja etykieta
        public string tekst; // Pomocnicza zmienna, nie pamiętam do czego xD
        
        public Guy(string Name, Bet MyBet, int Cash, RadioButton MyRadioButton, Label MyLabel)
        {
            this.Name = Name;
            this.MyBet = MyBet;
            this.Cash = Cash;
            this.MyRadioButton = MyRadioButton;
            this.MyLabel = MyLabel;
        }
        /**
         * Ustaw moje pole tekstowe na opis zakładu, a napis obok pola wyboru  
         * na ilość pieniędzy (np. "Janek ma 40zł")
         **/
        public void UpdateLabels()
        {
            if (MyBet == null)
            {
                MyLabel.Text = (Name + "nie zawarł zakładu.");
            }
            else
            {
                MyLabel.Text = MyBet.GetDescription();
            }
            MyRadioButton.Text = (Name + " ma " + Cash + "zł");
        }
        // Wyczyść mój zakład aby był równy 0
        public void ClearBer()
        {
            MyBet.Amount = 0;
        }
        /**
         * Ustal nowy zakład i przechowuj go w polu MyBet
         * Zwróc true, jezeli facet ma wystarczającą ilość pieniędzy, aby obstawić
         **/
        public bool PlaceBet(int Amount, int DogToWin)
        {
            if (Amount <= Cash)
            {
                MyBet = new Bet(Amount, DogToWin, this);
                return true;
            }
            return false;
        }
        // Poproś o wypłatę zakładu i zaktualizuj etykiety
        public void Collect(int Winner)
        {
            Cash = Cash + MyBet.PayOut(Winner);
            UpdateLabels();
        }
        /**
         * Kod do wyświetlania Name w polu "Janek postawił 5zł na psa numer"
         * bez tego wyświetla sie "Lab1.Guy postawił..."
         **/
        public override string ToString()
        {
            return Name;
        }
    }
}
