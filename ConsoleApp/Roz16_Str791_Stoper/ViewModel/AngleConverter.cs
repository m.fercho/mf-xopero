﻿using System;
using System.Windows.Data;

namespace Roz16_Str791_Stoper.ViewModel
{
    class AngleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            double parsedValue;
            if ((value != null) && double.TryParse(value.ToString(), out parsedValue) && (parameter != null))
                switch (parameter.ToString())
                {
                    case "Hours":
                        return parsedValue * 30;
                    case "Minutes":
                    case "Seconds":
                        return parsedValue * 6;
                }
            return 0;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}