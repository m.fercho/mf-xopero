﻿using System;
using System.Windows.Data;
using System.Globalization;

namespace Roz16_Str791_Stoper.ViewModel
{
    class TimeNumberFormatConverter : IValueConverter
    {
        public object ConvertBack(Object value, Type targetType,
            Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object Convert(Object value, Type targetType,
            Object parameter, CultureInfo culture)
        {
            if (value is decimal)
            {
                return ((decimal)value).ToString("00.00");
            }
            else if (value is int)
            {
                if (parameter == null)
                {
                    return ((int)value).ToString("d1");
                }
                else
                {
                    return ((int)value).ToString(parameter.ToString());
                }
            }
            return value;
        }
    }
}
