﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace Roz16_Str791_Stoper.ViewModel
{
    class BoleanVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, 
            CultureInfo culture)
        {
            if ((value is bool) && ((bool)value) == true)
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, 
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
