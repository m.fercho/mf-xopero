﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace Roz13_Str666_Birthday
{
    class RobustGuy
    {
        public DateTime? Birthday { get; private set; }
        public int? Height { get; private set; }
        public RobustGuy(string birthday, string height)
        {
            if (DateTime.TryParse(birthday, out DateTime tempDate))
                Birthday = tempDate;
            else Birthday = null;
            if (int.TryParse(height, out int tempInt))
                Height = tempInt;
            else Height = null;
        }
        public override string ToString()
        {
            string description;
            if (Birthday.HasValue)
                description = "Urodziłem się w " + Birthday.Value.ToLongDateString();
            else
                description = "Nie znam daty swoich urodzin.";
            if (Height.HasValue)
                description += ", mam " + Height + " centymetrów wzrtostu.";
            else description += ", nie wiem ile mam wzrostu :(. ";
            return description;
        }

    }
}
