﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz13_Str666_Birthday
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("---------------------------------------------------------"
                + Environment.NewLine
                + "Witaj w aplikacji konsolowej, z zadaniem ze strony 666."
                + Environment.NewLine
                + "Podaj datę urodzenia: ");
            string birthday = Console.ReadLine();
            Console.Write("Okresl swój wzrost (w centymetrach): ");
            string height = Console.ReadLine();
            RobustGuy guy = new RobustGuy(birthday, height);
            Console.WriteLine(guy.ToString() + Environment.NewLine
                + "---------------------------------------------------------"
                + Environment.NewLine
                + "Wciśnij dowolny klawisz, aby zakończyć działanie programu.");
            Console.ReadKey();
        }
    }
}
