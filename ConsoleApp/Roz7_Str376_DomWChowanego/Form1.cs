﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Roz7_Str376_DomWChowanego
{
    public partial class Form1 : Form
    {
        int Moves;
        Location currentLocation;
        OutsideWithDoor frontYard, backYard;
        OutsideWithHidingPlace driveway, garden;
        Room diningRoom, stairs;
        RoomWithDoor kitchen, livingRoom;
        RoomWithHidingPlace hallway, bathroom, masterBedroom, secondBedroom;
        Opponent opponent;

        public Form1()
        {
            InitializeComponent();
            CreateObject();
            MovieToANewLocation(livingRoom);
            opponent = new Opponent(frontYard);
            ResetGame(false);
        }
        private void CreateObject()
        {
            frontYard = new OutsideWithDoor("Podwórko przed domem", false, "dębowe drzwi z mosiężną klamką");
            backYard = new OutsideWithDoor("Podwórko za domem", true, "rozsuwane drzwi");
            diningRoom = new Room("Jadalnia", "kryształowy żyrandol");
            stairs = new Room("schody", "Drewniana poręcz");
            livingRoom = new RoomWithDoor("Salon", "antyczny dywan", "dębowe drzwi z mosiężną klamką", "w szafie ściennej");
            kitchen = new RoomWithDoor("Kuchnia", "nierdzewne stalowe sztućce", "rozsuwane drzwi", "w szafce");
            hallway = new RoomWithHidingPlace("Korytarz na górze", "Obrazek z psem", "w szafie ściennej");
            bathroom = new RoomWithHidingPlace("Łazienka", "umywalka i toaleta", "pod prysznicem");
            masterBedroom = new RoomWithHidingPlace("Duża sypialnia", "duże łóżko", "pod łóżkiem");
            secondBedroom = new RoomWithHidingPlace("Druga sypialnia", "małe łóżko", "pod łóżkiem");
            garden = new OutsideWithHidingPlace("Ogród", false, "w szopie");
            driveway = new OutsideWithHidingPlace("Droga dojazdowa", true, "w garażu");

            garden.Exits = new Location[] { frontYard, backYard };
            frontYard.Exits = new Location[] { garden, livingRoom };
            backYard.Exits = new Location[] { garden, kitchen };
            diningRoom.Exits = new Location[] { livingRoom, kitchen };
            stairs.Exits = new Location[] { livingRoom, hallway };
            livingRoom.Exits = new Location[] { frontYard, diningRoom };
            kitchen.Exits = new Location[] { backYard, diningRoom };
            hallway.Exits = new Location[] { stairs, bathroom, masterBedroom, secondBedroom };
            bathroom.Exits = new Location[] { hallway };
            masterBedroom.Exits = new Location[] { hallway };
            secondBedroom.Exits = new Location[] { hallway };
            garden.Exits = new Location[] { backYard, frontYard };
            driveway.Exits = new Location[] { backYard, frontYard };
            livingRoom.DoorLocation = frontYard;
            frontYard.DoorLocation = livingRoom;
            kitchen.DoorLocation = backYard;
            backYard.DoorLocation = kitchen;
        }
        private void MovieToANewLocation(Location newLocation)
        {
            Moves++;
            currentLocation = newLocation;
            RedrawForm();
        }
        private void RedrawForm()
        {
            exits.Items.Clear();
            for (int i = 0; i < currentLocation.Exits.Length; i++)
            {
                exits.Items.Add(currentLocation.Exits[i].Name);
            }
            exits.SelectedIndex = 0;
            description.Text = currentLocation.Description + "\r\n (Ruch numer " + Moves + ")";
            if (currentLocation is IHidingPlace)
            {
                IHidingPlace hidingPlace = currentLocation as IHidingPlace;
                check.Text = "Sprawdź " + hidingPlace.HidingPlaceName;
                check.Enabled = true;
            }
            else
            {
                check.Enabled = false;
            }
            if (currentLocation is IHasExteriorDoor)
            {
                goTroughTheDoor.Enabled = true;
            }
            else
            {
                goTroughTheDoor.Enabled = false;
            }
        }
        private void check_Click(object sender, EventArgs e)
        {
            Moves++;
            if (opponent.Check(currentLocation))
            {
                ResetGame(true);
            }
            else
            {
                RedrawForm();
            }
        }

        private void hide_Click(object sender, EventArgs e)
        {
            hide.Enabled = false;
            for (int i = 0; i <= 10; i++)
            {
                opponent.Move();
                description.Text = i + "...";
                Application.DoEvents();
                Thread.Sleep(200);
            }
            description.Text = "Gotowy czy nie, szukam!";
            Application.DoEvents();
            Thread.Sleep(500);
            goHere.Enabled = true;
            exits.Enabled = true;
            MovieToANewLocation(livingRoom);
        }

        private void goHere_Click(object sender, EventArgs e)
        {
            MovieToANewLocation(currentLocation.Exits[exits.SelectedIndex]);
        }

        private void goTroughTheDoor_Click(object sender, EventArgs e)
        {
            IHasExteriorDoor hasDoor = currentLocation as IHasExteriorDoor;
            MovieToANewLocation(hasDoor.DoorLocation);
        }
        private void ResetGame(bool displayMessage)
        {
        if (displayMessage)
            {
                MessageBox.Show("Znalazłes mnie w liczbie ruchówe równej: " + Moves + ".", "Wygrałeś...");
                IHidingPlace foundLocation = currentLocation as IHidingPlace;
                description.Text = "Znalazłes przeciwnika w liczbie ruchów równej: " + Moves
                    + ". \r\n Ukrywał się w " + foundLocation.HidingPlaceName + ". ";
            }
        }
    }
}
