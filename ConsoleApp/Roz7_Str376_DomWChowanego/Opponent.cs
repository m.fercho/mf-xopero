﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str376_DomWChowanego
{   
    class Opponent
    {
        private Location myLocation;
        private Random myRandom;
        public Opponent(Location startLocation)
        {
            myLocation = startLocation;
            myRandom = new Random();
        }
        public void Move()
        {
            bool hidden = false;
            while (!hidden)
            {

                if (myLocation is IHasExteriorDoor)
                {
                    IHasExteriorDoor locationWithDoor =
                                    myLocation as IHasExteriorDoor;
                    if (myRandom.Next(2) == 1)
                        myLocation = locationWithDoor.DoorLocation;
                }
                int rand = myRandom.Next(myLocation.Exits.Length);
                myLocation = myLocation.Exits[rand];
                hidden = true;
            }
        }
        public bool Check(Location locationToCheck)
        {
            if (locationToCheck == myLocation)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
