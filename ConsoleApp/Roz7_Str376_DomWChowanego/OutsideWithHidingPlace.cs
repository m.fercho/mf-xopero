﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str376_DomWChowanego
{
    class OutsideWithHidingPlace : Outside
    {
        public string HidingPlaceName { get; private set; }
        public OutsideWithHidingPlace(string name, bool hot, string hidinhPlaceName)
            : base(name, hot)
        {
            HidingPlaceName = hidinhPlaceName;
        }
        public override string Description
        {
            get
            {
                return base.Description + " \r\n Ktoś może ukrywać się w " + HidingPlaceName + ".";
            }
        }

    }
}
