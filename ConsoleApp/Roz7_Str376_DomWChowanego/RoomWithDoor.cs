﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str376_DomWChowanego
{
    class RoomWithDoor : RoomWithHidingPlace, IHasExteriorDoor
    {
        public string DoorDescription { get; private set; }
        public Location DoorLocation { get; set; }
        public RoomWithDoor(string name, string decoration, string doorDescirption, string hidingPlaceName) 
            : base(name, decoration, hidingPlaceName)
        {
            DoorDescription = doorDescirption;
        }
    }
}
