﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str376_DomWChowanego
{
    class Location
    {
        public string Name { get; private set; }
        public Location[] Exits;
        public Location(string name)
        {
            Name = name;
        }
        public virtual string Description
        {
            get
            {
                string description = " Stoisz w: " + Name +
                    ".\r\n Widzisz wyjście do następujących lokacji: ";
                for (int i = 0; i < Exits.Length; i++)
                {
                    description += " \r\n " + Exits[i].Name;
                    if (i != Exits.Length - 1)
                    {
                        description += ",";
                    }
                }
                description += ".";
                return description;
            }
        }
    }
}
