﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Roz15_Str740_Baseball
{
    class Fan
    {
        public ObservableCollection<string> FanSays = new ObservableCollection<string>();
        private int pitchNumber = 0;
        public Fan(Ball ball)
        {
            ball.BallInPlay += new EventHandler(ball_BallInPlay);
        }

        private void ball_BallInPlay(object sender, EventArgs e)
        {
            if (e is BallEventArgs)
            {
                BallEventArgs ballEventArgs = e as BallEventArgs;
                if (ballEventArgs.Distance > 120 && ballEventArgs.Trajectory > 30)
                {
                    FanSays.Add("Rzut #" + pitchNumber + ": Home run! Idę po piłkę");
                }
                else
                {
                    FanSays.Add("Rzut #" + pitchNumber + ": Woo-hoo! Yeah!");
                }
                pitchNumber += 1;
            }
        }
    }
}
