﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Roz15_Str740_Baseball
{
    class Pitcher
    {
        public ObservableCollection<string> PitcherSays = new ObservableCollection<string>();
        private int pitchNumber = 0;

        public Pitcher(Ball ball)
        {
            ball.BallInPlay += new EventHandler(ball_BallInPlay);
        }

        private void ball_BallInPlay(object sender, EventArgs e)
        {
            if (e is BallEventArgs)
            {
                BallEventArgs ballEventArgs = e as BallEventArgs;
                if ((ballEventArgs.Distance < 29) && (ballEventArgs.Trajectory < 60))
                {
                    CatchBall();
                }
                else
                {
                    CoverFirstBase();
                }
                pitchNumber += 1;
            }
        }
        private void CatchBall()
        {
            PitcherSays.Add("Rzut #" + pitchNumber + ": Złapałem piłkę");
        }
        private void CoverFirstBase()
        {
            PitcherSays.Add("Rzut #" + pitchNumber + ": Pokryłem pierwsza bazę");
        }
    }
}
