﻿namespace Roz13_Str648_CloneGCcollect
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnlone1 = new System.Windows.Forms.Button();
            this.btnClone2 = new System.Windows.Forms.Button();
            this.btnGCcollect = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnlone1
            // 
            this.btnlone1.Location = new System.Drawing.Point(12, 12);
            this.btnlone1.Name = "btnlone1";
            this.btnlone1.Size = new System.Drawing.Size(337, 88);
            this.btnlone1.TabIndex = 0;
            this.btnlone1.Text = "Klon 1";
            this.btnlone1.UseVisualStyleBackColor = true;
            this.btnlone1.Click += new System.EventHandler(this.btnlone1_Click);
            // 
            // btnClone2
            // 
            this.btnClone2.Location = new System.Drawing.Point(12, 106);
            this.btnClone2.Name = "btnClone2";
            this.btnClone2.Size = new System.Drawing.Size(337, 88);
            this.btnClone2.TabIndex = 1;
            this.btnClone2.Text = "Klon 2";
            this.btnClone2.UseVisualStyleBackColor = true;
            this.btnClone2.Click += new System.EventHandler(this.btnClone2_Click);
            // 
            // btnGCcollect
            // 
            this.btnGCcollect.Location = new System.Drawing.Point(12, 200);
            this.btnGCcollect.Name = "btnGCcollect";
            this.btnGCcollect.Size = new System.Drawing.Size(337, 88);
            this.btnGCcollect.TabIndex = 2;
            this.btnGCcollect.Text = "GC Collect";
            this.btnGCcollect.UseVisualStyleBackColor = true;
            this.btnGCcollect.Click += new System.EventHandler(this.btnGCcollect_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 308);
            this.Controls.Add(this.btnGCcollect);
            this.Controls.Add(this.btnClone2);
            this.Controls.Add(this.btnlone1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Klony";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnlone1;
        private System.Windows.Forms.Button btnClone2;
        private System.Windows.Forms.Button btnGCcollect;
    }
}

