﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz13_Str648_CloneGCcollect
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnlone1_Click(object sender, EventArgs e)
        {
            using (Clone clone1 = new Clone(1))
            {
                // nic nie rób!
            }
        }

        private void btnClone2_Click(object sender, EventArgs e)
        {
            Clone clone2 = new Clone(2);
            clone2 = null;
        }

        private void btnGCcollect_Click(object sender, EventArgs e)
        {
            GC.Collect();
        }
    }
}
