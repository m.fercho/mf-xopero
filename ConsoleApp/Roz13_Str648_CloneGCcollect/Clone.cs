﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Roz13_Str648_CloneGCcollect
{
    [Serializable]
    class Clone : IDisposable
    {
        string fileName = @"C:\Users\mat3u\Documents\xopero\supportFolder\Klon.dat";
        string dirName = @"C:\Users\mat3u\Documents\xopero\supportFolder\";
        public int Id { get; private set; }

        public Clone(int Id)
        {
            this.Id = Id;
        }

        public void Dispose()
        {
            if (File.Exists(fileName) == false)
            {
                Directory.CreateDirectory(dirName);
            }
            BinaryFormatter bf = new BinaryFormatter();
            using (Stream output = File.OpenWrite(fileName))
            {
                bf.Serialize(output, this);
            }
            MessageBox.Show("Must...serialize...object!", "Clone #" + Id + " says...");
        }

        ~Clone()
        {
            MessageBox.Show("Aaaaaa! Dopadłeś mnie!",
                       "Klon " + Id + ". mówi...");
        }
    }

}
