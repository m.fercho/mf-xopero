﻿using System;
using System.IO;

namespace Roz9_Str446_Kanciarz
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter sw = new StreamWriter(@"C:\Users\mat3u\Documents\xopero\tajny_plan.txt");
            sw.WriteLine("W jaki sposób pokonać Kapitana Wspaniałego?");
            sw.WriteLine("Kolejny genialny, tajny plan Kanciarza.");
            sw.Write("Stworzę armię klonów, ");
            sw.WriteLine("uwolnię je i wystawię przeciwko mieszkańcom Obiektowa.");
            string location = "centrum handlowe";
            for (int number = 0; number <= 6; number++)
            {
                sw.WriteLine("Klon numer {0} atakuje {1}", number, location);
                if (location == "centrum handlowe") { location = "centrum miasta"; }
                else { location = "centrum handlowe"; }
            }
            sw.Close();
        }
    }
}
