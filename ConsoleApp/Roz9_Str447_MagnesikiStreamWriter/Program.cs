﻿using System;
using System.IO;

namespace Roz9_Str447_MagnesikiStreamWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            Flobbo f = new Flobbo("niebiesko-żółta");
            StreamWriter sw = f.Snobbo();
            f.Blobbo(f.Blobbo(f.Blobbo(sw), sw), sw);
        }
    }
    class Flobbo
    {
        private string zap;
        public Flobbo(string zap)
        {
            this.zap = zap;
        }
        public StreamWriter Snobbo()
        {
            return new
               StreamWriter("ara.txt");
        }
        public bool Blobbo(StreamWriter sw)
        {
            sw.WriteLine(zap);
            zap = "zielono-purpurowa";
            return false;
        }
        public bool Blobbo(bool Already, StreamWriter sw)
        {
            if (Already)
            {
                sw.WriteLine(zap);
                sw.Close();
                return false;
            }
            else
            {
                sw.WriteLine(zap);
                zap = "czerwono-pomarańczowa";
                return true;
            }
        }
    }
}
