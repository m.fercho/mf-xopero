﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Roz9_Str483
{
    class Program
    {
        static void Main(string[] args)
        {
            int intVal = 48769414;
            string stringVal = "Hello!";
            byte[] byteArray = { 47, 129, 0, 116 };
            float floatVal = 491.695F;
            char charVal = 'E';
            void WriterThing()
            {
                using (FileStream output = File.Create("daneBinarne.dat"))
                using (BinaryWriter writer = new BinaryWriter(output))
                {
                    writer.Write(intVal);
                    writer.Write(stringVal);
                    writer.Write(byteArray);
                    writer.Write(floatVal);
                    writer.Write(charVal);
                }
                byte[] dataWritten = File.ReadAllBytes("daneBinarne.dat");
                foreach (byte b in dataWritten)
                {
                    Console.Write("{0:x2} ", b);
                }
                Console.WriteLine(" - {0} bajtów ", dataWritten.Length);
                Console.ReadKey();
            }
            void ReaderThing()
            {
                using (FileStream input = File.OpenRead("daneBinarne.dat"))
                using (BinaryReader reader = new BinaryReader(input))
                {
                    int intReed = reader.ReadInt32(); 
                    string stringRead = reader.ReadString(); 
                    byte[] byteArrayRead = reader.ReadBytes(4); 
                    float floatRead = reader.ReadSingle(); 
                    char charRead = reader.ReadChar();
                    Console.Write("int: {0}  string: {1}  bytes: ", intReed, stringRead); foreach (byte b in byteArrayRead) Console.Write("{0} ", b); Console.Write(" float: {0}  char: {1} ", floatRead, charRead);
                }
                Console.ReadKey();
            }
         //   WriterThing();
            ReaderThing();
        }
    }
}
