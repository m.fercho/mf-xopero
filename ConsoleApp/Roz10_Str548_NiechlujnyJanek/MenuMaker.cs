﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Roz10_Str548_NiechlujnyJanek 
{
    class MenuMaker : INotifyPropertyChanged
    {
        private Random random = new Random();
        private List<String> meats = new List<String>()
        {
            "Pieczona wołowina",
            "Salami",
            "Indyk",
            "Szynka",
            "Karkówka", 
        };
        private List<String> condiments = new List<String>()
        {
            "Musztarda żółta",
            "Musztrda brązowa",
            "Musztarda miodowa",
            "Majonez",
            "Przyprawa",
            "Sos francuski", 
        };
        private List<String> breads = new List<String>()
        {
            "Chleb ryżowy",
            "Chleb biały",
            "Chleb zbożowy",
            "Pumpernikiel",
            "Chleb włoski",
            "Bułka", 
        };
        public ObservableCollection<MenuItem> Menu { get; private set; }
        public DateTime GeneratedDate { get; set; }
        public int NumberOfItems { get; set; }
        public MenuMaker()
        {
            Menu = new ObservableCollection<MenuItem>();
            NumberOfItems = 10;
            UpdateMenu();
        }
        private MenuItem CreateMenuItem()
        {
            string randomMeat = meats[random.Next(meats.Count)];
            string randomCondiment = condiments[random.Next(condiments.Count)];
            string randomBread = breads[random.Next(breads.Count)];
            return new MenuItem(randomMeat, randomCondiment, randomBread);
        }
        public void UpdateMenu()
        {
            Menu.Clear();
            for (int i = 0; i < NumberOfItems; i++)
            {
                Menu.Add(CreateMenuItem());
            }
            GeneratedDate = DateTime.Now;
            OnPropertyChanged("GeneratedDate");
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChangedEvent = PropertyChanged;
            if (propertyChangedEvent != null)
            {
                propertyChangedEvent(this, new PropertyChangedEventArgs(propertyName));
            }
        }


    }
}
