﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    abstract class Weapon : Mover
    {
        public abstract string Name { get; }
        public bool PickedUp { get; private set; }
        private Size enemySize = new Size(50, 50);

        public Weapon(Game game, Point location) : base(game, location)
        {
            PickedUp = false;
        }
        public void PickUpWeapon()
        {
            PickedUp = true;
        }
        public abstract void Attack(Direction direction, Random random);


        private Point Move(Direction direction, Point target, Rectangle boundaries)
        {
            target = Move(direction, boundaries);
            return target;
        }

        public bool Nearby(Point locationToCheck, Point target, Direction direction, int distance)
        {
            bool isNearby = false;
            Rectangle enemyBoundary = new Rectangle(target, enemySize);
            Rectangle playerAttackArea = new Rectangle();
            switch (direction)
            {
                case Direction.Up:
                playerAttackArea.Location = new Point(locationToCheck.X, locationToCheck.Y - distance);
                playerAttackArea.Width = enemySize.Width;
                playerAttackArea.Height = distance;
                break;
                case Direction.Right:
                playerAttackArea.Location = new Point(locationToCheck.X + enemySize.Width, locationToCheck.Y);
                playerAttackArea.Width = distance;
                playerAttackArea.Height = enemySize.Height;
                break;
                case Direction.Down:
                playerAttackArea.Location = new Point(locationToCheck.X, locationToCheck.Y + enemySize.Height);
                playerAttackArea.Width = enemySize.Width;
                playerAttackArea.Height = distance;
                break;
                case Direction.Left:
                playerAttackArea.Location = new Point(locationToCheck.X + distance, locationToCheck.Y);
                playerAttackArea.Width = distance;
                playerAttackArea.Height = enemySize.Height;
                break;
            }

            if (playerAttackArea.IntersectsWith(enemyBoundary))
                isNearby = true;
            return isNearby;
        }

        protected bool DamageEnemy(Direction direction, int radius, int damage, Random random)
        {
            Point target = game.PlayerLocation;
            for (int distance = 0; distance < radius; distance++)
            {
                foreach (Enemy enemy in game.Enemies)
                {
                    if (Nearby(enemy.Location, target, direction, distance))
                    {
                        enemy.Hit(damage, random);
                        return true;
                    }
                }
                target = Move(direction, target, game.Boundaries);
            }
            return false;
        }


        protected Direction RightDirection(Direction direction)
        {
            Direction RightDirection = direction;

            switch (direction)
            {
                case Direction.Up:
                RightDirection = Direction.Right;
                break;
                case Direction.Right:
                RightDirection = Direction.Down;
                break;
                case Direction.Down:
                RightDirection = Direction.Left;
                break;
                case Direction.Left:
                RightDirection = Direction.Up;
                break;
            }
            return RightDirection;
        }


        protected Direction LeftDirection(Direction direction)
        {
            Direction LeftDirection = direction;
            switch (direction)
            {
                case Direction.Up:
                LeftDirection = Direction.Left;
                break;
                case Direction.Right:
                LeftDirection = Direction.Up;
                break;
                case Direction.Down:
                LeftDirection = Direction.Right;
                break;
                case Direction.Left:
                LeftDirection = Direction.Down;
                break;
            }
            return LeftDirection;
        }
    }
}
