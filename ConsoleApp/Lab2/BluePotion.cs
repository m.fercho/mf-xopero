﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab2
{
    class BluePotion : Weapon, IPotion
    {
        public bool Used { get; private set; }
        public override string Name { get { return "Niebieska mikstura"; } }
        public BluePotion(Game game, Point location) : base(game, location)
        {
            Used = false;
        }
        public override void Attack(Direction direction, Random random)
        {
            game.IncreasePlayerHealth(5, random);
            Used = true;
        }
    }
}
