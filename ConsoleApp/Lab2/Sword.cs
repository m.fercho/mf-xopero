﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab2
{
    class Sword : Weapon
    {
        private int SwordRadius = 10;
        private int SwordDamage = 3;
        public Sword(Game game, Point location) : base (game, location) { }
        public override string Name { get { return "Miecz"; } }
        public override void Attack(Direction direction, Random random)
        {
            if (!DamageEnemy(direction, SwordRadius, SwordDamage, random))
            {
                if (!DamageEnemy(RightDirection(direction), SwordRadius, SwordDamage, random))
                {
                    DamageEnemy(LeftDirection(direction), SwordRadius, SwordDamage, random);
                }
            }
        }
    }
}
