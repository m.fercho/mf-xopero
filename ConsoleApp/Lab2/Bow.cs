﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab2
{
    class Bow : Weapon
    {
        private int BowRadius = 30;
        private int BowDamage = 1;
        public Bow(Game game, Point location) : base(game, location) { }
        public override string Name { get { return "Łuk"; } }
        public override void Attack(Direction direction, Random random)
        {
            if (!DamageEnemy(direction, BowRadius, BowDamage, random))
            {
                if (!DamageEnemy(RightDirection(direction), BowRadius, BowDamage, random))
                {
                    DamageEnemy(LeftDirection(direction), BowRadius, BowDamage, random);
                }
            }
        }
    }
}
