﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace Lab2
{
    class Game
    {
        public IEnumerable<Enemy> Enemies { get; private set; }
        public Weapon WeaponInRoom { get; private set; }
        private Player player;
        private int level = 0;
        public Point PlayerLocation { get { return player.Location; } }
        public int PlayerHitPoints { get { return player.HitPoints; } }
        
        private Rectangle boundaries;
        public Rectangle Boundaries { get { return boundaries; } }

        public Game(Rectangle boundaries)
        {
            this.boundaries = boundaries;
            player = new Player(this, new Point(boundaries.Left + 10, boundaries.Top + 70));
        }
        public void Move(Direction direction, Random random)
        {
            player.Move(direction);
            foreach (Enemy enemy in Enemies)
                enemy.Move(random);
        }
        public void Equip(string weaponName)
        {
            player.Equip(weaponName);
        }
        public bool CheckPlayerInventory(string weaponName)
        {
            return player.Weapons.Contains(weaponName);
        }
        public void HitPlayer(int maxDamage, Random random)
        {
            player.Hit(maxDamage, random);
        }
        public void IncreasePlayerHealth(int health, Random random)
        {
            player.IncreaseHealth(health, random);
        }
        public void Attack(Direction direction, Random random)
        {
            player.Attack(direction, random);
            foreach (Enemy enemy in Enemies)
                enemy.Move(random);
        }
        private Point GetRandomLocation(Random random)
        {
            int x = boundaries.Left
                + random.Next(boundaries.Right / 10 - boundaries.Left / 10) * 10;
            int y = boundaries.Top
                + random.Next(boundaries.Bottom / 10 - boundaries.Top / 10) * 10;
            return new Point(x, y);
        }

        // Wartości bool potrzebne do metody "CheckPlayerPotion()" w klasie formularza. 
        public bool IsWeaponEquipped(string weaponName)
        {
            return player.IsWeaponEquipped(weaponName);
        }
        public bool CheckPotionUsed(string potionName)
        {
            return player.CheckPotionUsed(potionName);
        }

        // Nowe, kolejne poziomy
        public void NewLevel(Random random)
        {
            level++;
            switch (level)
            {
                // Poziom 1
                case 1:
                Enemies = new List<Enemy>()
                {
                    new Bat(this, GetRandomLocation(random)),
                };
                WeaponInRoom = new Sword(this, GetRandomLocation(random));
                break;
                // Poziom 2
                case 2:
                Enemies = new List<Enemy>()
                {
                    new Ghost(this, GetRandomLocation(random)),
                };
                WeaponInRoom = new BluePotion(this, GetRandomLocation(random));
                break;
                // Poziom 3
                case 3:
                Enemies = new List<Enemy>()
                {
                    new Ghoul(this, GetRandomLocation(random)),
                };
                WeaponInRoom = new Bow(this, GetRandomLocation(random));

                break;
                // Poziom 4
                case 4:
                Enemies = new List<Enemy>()
                {
                    new Bat(this, GetRandomLocation(random)),
                    new Ghost(this, GetRandomLocation(random)),
                };
                if (CheckPlayerInventory("Łuk"))
                {
                    if (!CheckPlayerInventory("Niebieska mikstura") || CheckPlayerInventory("Niebieska mikstura") && player.CheckPotionUsed("Niebieska mikstura"))
                    {
                        WeaponInRoom = new BluePotion(this, GetRandomLocation(random));
                    }
                }
                else
                {
                    WeaponInRoom = new Bow(this, GetRandomLocation(random));
                }
                break;
                // Poziom 5
                case 5:
                Enemies = new List<Enemy>()
                {
                    new Bat(this, GetRandomLocation(random)),
                    new Ghoul(this, GetRandomLocation(random)),
                };
                WeaponInRoom = new RedPotion(this, GetRandomLocation(random));
                break;
                // Poziom 6
                case 6:
                Enemies = new List<Enemy>()
                {
                    new Ghost(this, GetRandomLocation(random)),
                    new Ghoul(this, GetRandomLocation(random)),
                };
                WeaponInRoom = new Mace(this, GetRandomLocation(random));
                break;
                // Poziom 7
                case 7:
                Enemies = new List<Enemy>()
                {
                    new Bat(this, GetRandomLocation(random)),
                    new Ghost(this, GetRandomLocation(random)),
                    new Ghoul(this, GetRandomLocation(random)),
                };
                if (CheckPlayerInventory("Mortgensztern"))
                {
                    if (!CheckPlayerInventory("Czerwona mikstura") || CheckPlayerInventory("Czerwona mikstura") && player.CheckPotionUsed("Czerwona mikstura"))
                    {
                        WeaponInRoom = new RedPotion(this, GetRandomLocation(random));
                    }
                }
                else
                {
                    WeaponInRoom = new Mace(this, GetRandomLocation(random));
                }
                break;
                // Koniec gry 
                case 8:
                Application.Exit();
                DialogResult result = MessageBox.Show("Brawo. Dotarłeś do końca lochu, aby odebrać nagrode wciśnij ok.", "Yupi!", MessageBoxButtons.OK);
                if (result == DialogResult.OK)
                {

                }
                break;
            }
        }
    }
}
