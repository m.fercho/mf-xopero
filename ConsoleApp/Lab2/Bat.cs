﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab2
{
    class Bat : Enemy
    {
        public Bat(Game game, Point location) : base(game, location, 6) { }
        public override void Move(Random random)
        {
            // Ruch w strone gracza
            if (random.Next(1, 2) == 1 && HitPoints > 0)
            {
                location = Move(FindPlayerDirection(game.PlayerLocation), game.Boundaries);
            }
            // Ruch w losowym kierunku
            else
            {
                location = Move((Direction)random.Next(1, 4), game.Boundaries);
            }
            // Atak
            if (NearPlayer())
            {
                game.HitPlayer(2, random);
            }
        }
    }
}
