﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class Form1 : Form
    {
        private Game game;
        private Random random = new Random();
        private bool isPotionNeeded = false;

        public Form1()
        {
            InitializeComponent();
        }

        // Obsługa zdarzenia Load formularza, deklaracja wymiarów lochów itp.
        private void Form1_Load(object sender, EventArgs e)
        {
            BeginSetup();
            UpdateCharacters();
        }
        
        // Początkowe ustawienia pola do gry (lochu)
        // oraz nakładania na siebie kontrolek
        private void BeginSetup()
        {
            game = new Game(new Rectangle(78, 57, 600, 225));
            game.NewLevel(random);
            player.BringToFront();
            bat.SendToBack();
            ghost.SendToBack();
            ghoul.SendToBack();
            bowPic.SendToBack();
            macePic.SendToBack();
            swordPic.SendToBack();
            bluePotPic.SendToBack();
            redPotPic.SendToBack();
        }

        // Początkowe ustawiwnie (nie)widoczności wszystkich kontrolek w lochu
        private void PictureBoxVisibility()
        {
            bat.Visible = false;
            ghost.Visible = false;
            ghoul.Visible = false;
            bowPic.Visible = false;
            macePic.Visible = false;
            swordPic.Visible = false;
            bluePotPic.Visible = false;
            redPotPic.Visible = false;
        }

        // Ustawiwnie (nie)widoczności broni w EQ
        private void InventoryItemsVisible()
        {
            inventorySword.Visible = false;
            inventoryBow.Visible = false;
            inventoryMorgensztern.Visible = false;
            inventoryRedPot.Visible = false;
            inventoryBluePot.Visible = false;
        }

        // Aktualizacja... wszystkiego 
        public void UpdateCharacters()
        {
            PictureBoxVisibility();
            player.Location = game.PlayerLocation;
            playerHitPoints.Text = game.PlayerHitPoints.ToString();
            Control weaponControl = null;
            bool showBat = false;
            bool showGhost = false;
            bool showGhoul = false;
            int enemiesShown = 0;

            // Pętelka do przeciwników
            foreach (Enemy enemy in game.Enemies)
            {
                if (enemy is Bat)
                {
                    bat.Location = enemy.Location;
                    batHitPoints.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showBat = true;
                        bat.Visible = showBat;
                        enemiesShown++;
                    }
                    else
                    {
                        showBat = false;
                        bat.Visible = showBat;
                        batHitPoints.Text = "0";
                    }
                }
                if (enemy is Ghost)
                {
                    ghost.Location = enemy.Location;
                    ghostHitPoints.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showGhost = true;
                        ghost.Visible = showGhost;
                        enemiesShown++;
                    }
                    else
                    {
                        showGhost = false;
                        ghost.Visible = showGhost;
                        ghostHitPoints.Text = "0";
                    }
                }
                if (enemy is Ghoul)
                {
                    ghoul.Location = enemy.Location;
                    ghoulHitPoints.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showGhoul = true;
                        ghoul.Visible = showGhoul;
                        enemiesShown++;
                    }
                    else
                    {
                        showGhoul = false;
                        ghoul.Visible = showGhoul;
                        ghoulHitPoints.Text = "0";
                    }
                }
            }

            InventoryItemsVisible();
            switch (game.WeaponInRoom.Name)
            {
                case "Miecz":
                weaponControl = swordPic;
                break;
                case "Łuk":
                weaponControl = bowPic;
                break;
                case "Morgensztern":
                weaponControl = macePic;
                break;
                case "Czerwona mikstura":
                weaponControl = redPotPic;
                break;
                case "Niebieska mikstura":
                weaponControl = bluePotPic;
                break;
                default:  break;
            }
            // Sprawdzacznie czy dany przedmiot jest w EQ
            weaponControl.Visible = true;
            if (game.CheckPlayerInventory("Miecz"))
            {
                inventorySword.Visible = true;
            }
            if (game.CheckPlayerInventory("Łuk"))
            {
                inventoryBow.Visible = true;
            }
            if (game.CheckPlayerInventory("Morgensztern"))
            {
                inventoryMorgensztern.Visible = true;
            }
            if (game.CheckPlayerInventory("Czerwona mikstura"))
            {
                inventoryRedPot.Visible = true;
            }
            if (game.CheckPlayerInventory("Niebieska mikstura"))
            {
                inventoryBluePot.Visible = true;
            }

            // Znikanie mikstury z EQ po uyciu
            CheckPlayerPotion("Niebieska mikstura", inventoryBluePot);
            CheckPlayerPotion("Czerwona mikstura", inventoryRedPot);

            // Ustawienie (nie)widoczności przedmiotu po podniesieniu w lochu
            weaponControl.Location = game.WeaponInRoom.Location;
            if (game.WeaponInRoom.PickedUp)
            {
                weaponControl.Visible = false;
            }
            else
            {
                weaponControl.Visible = true;
            }
            // Koniec gry
            if (game.PlayerHitPoints <= 0)
            {
                MessageBox.Show("Zostałeś zabity", "You die...");
                Application.Exit();
            }
            if (enemiesShown < 1)
            {
                MessageBox.Show("Pokonałeś wszystkich przeciwników na tym poziomie.", "You win...");
                game.NewLevel(random);
                UpdateCharacters();
            }

        }
        // EKWIPUNEK
        // MIECZ
        private void inventorySword_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Miecz"))
            {
                game.Equip("Miecz");
                inventorySword.BorderStyle = BorderStyle.FixedSingle;
                inventoryBow.BorderStyle = BorderStyle.None;
                inventoryMorgensztern.BorderStyle = BorderStyle.None;
                inventoryRedPot.BorderStyle = BorderStyle.None;
                inventoryBluePot.BorderStyle = BorderStyle.None;
            }
        }
        // ŁUK
        private void inventoryBow_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Łuk"))
            {
                game.Equip("Łuk");
                inventorySword.BorderStyle = BorderStyle.None;
                inventoryBow.BorderStyle = BorderStyle.FixedSingle;
                inventoryMorgensztern.BorderStyle = BorderStyle.None;
                inventoryRedPot.BorderStyle = BorderStyle.None;
                inventoryBluePot.BorderStyle = BorderStyle.None;
            }
        }
        // MORGENSZTERN (to co widać w książce to nie jest buława)
        private void inventoryMorgensztern_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Morgensztern"))
            {
                game.Equip("Morgensztern");
                inventorySword.BorderStyle = BorderStyle.None;
                inventoryBow.BorderStyle = BorderStyle.None;
                inventoryMorgensztern.BorderStyle = BorderStyle.FixedSingle;
                inventoryRedPot.BorderStyle = BorderStyle.None;
                inventoryBluePot.BorderStyle = BorderStyle.None;
            }
        }
        // CZERWONA MIKSTURA
        private void inventoryRedPot_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Czerwona mikstura"))
            {
                game.Equip("Czerwona mikstura");
                inventorySword.BorderStyle = BorderStyle.None;
                inventoryBow.BorderStyle = BorderStyle.None;
                inventoryMorgensztern.BorderStyle = BorderStyle.None;
                inventoryRedPot.BorderStyle = BorderStyle.FixedSingle;
                inventoryBluePot.BorderStyle = BorderStyle.None;
            }
        }
        // NIEBIESKA MIKSTURA
        private void inventoryBluePot_Click(object sender, EventArgs e)
        {
            if (game.CheckPlayerInventory("Niebieska mikstura"))
            {
                game.Equip("Niebieska mikstura");
                inventorySword.BorderStyle = BorderStyle.None;
                inventoryBow.BorderStyle = BorderStyle.None;
                inventoryMorgensztern.BorderStyle = BorderStyle.None;
                inventoryRedPot.BorderStyle = BorderStyle.None;
                inventoryBluePot.BorderStyle = BorderStyle.FixedSingle;
            }
        }
        // Przyciski
        // RUCH
        // GÓRA
        private void moveUp_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Up, random);
            UpdateCharacters();
        }
        // LEWO
        private void moveLeft_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Left, random);
            UpdateCharacters();
        }
        // DÓŁ
        private void moveDown_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Down, random);
            UpdateCharacters();
        }
        // PRAWO
        private void moveRight_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Right, random);
            UpdateCharacters();
        }
        // Przyciski
        // ATAK / WALKA
        // GÓRA
        private void attackUp_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Up, random);
            UpdateCharacters();
        }
        // LEWO
        private void attackLeft_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Left, random);
            UpdateCharacters();
        }
        // DÓŁ
        private void attackDown_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Down, random);
            UpdateCharacters();
        }
        // PRAWO
        private void attackRight_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Right, random);
            UpdateCharacters();
        }
        
        // Metoda do sprawdzania czy mikstura została uzyta 
        // jesli tak wyłączenie widoczności w EQ
        private void CheckPlayerPotion(string weaponName, PictureBox weaponPictureBox)
        {
            weaponPictureBox.BorderStyle = BorderStyle.None;
            if (game.CheckPlayerInventory(weaponName))
            {
                if (!game.CheckPotionUsed(weaponName))
                {
                    weaponPictureBox.Visible = true;
                    if (game.IsWeaponEquipped(weaponName))
                    {
                        weaponPictureBox.BorderStyle = BorderStyle.FixedSingle;
                        isPotionNeeded = true;
                    }
                }
                else
                {
                    weaponPictureBox.BorderStyle = BorderStyle.None;
                    weaponPictureBox.Visible = false;
                    if (isPotionNeeded)
                    {
                        game.Equip("Miecz");
                        isPotionNeeded = false;
                    }
                }
            }
        }
        
        /**A to są znaki skopiowane z tablicy znaków potrzebne do przycisków formularza, kolejno...
         ** Góra: ▲
         ** Lewo: ◄ 
         ** Dół: ▼
         ** Prawo: ►
         */
    }
}
