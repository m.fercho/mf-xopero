﻿namespace Lab2
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.player = new System.Windows.Forms.PictureBox();
            this.bat = new System.Windows.Forms.PictureBox();
            this.ghost = new System.Windows.Forms.PictureBox();
            this.ghoul = new System.Windows.Forms.PictureBox();
            this.redPotPic = new System.Windows.Forms.PictureBox();
            this.bluePotPic = new System.Windows.Forms.PictureBox();
            this.swordPic = new System.Windows.Forms.PictureBox();
            this.bowPic = new System.Windows.Forms.PictureBox();
            this.macePic = new System.Windows.Forms.PictureBox();
            this.inventorySword = new System.Windows.Forms.PictureBox();
            this.inventoryBow = new System.Windows.Forms.PictureBox();
            this.inventoryMorgensztern = new System.Windows.Forms.PictureBox();
            this.inventoryRedPot = new System.Windows.Forms.PictureBox();
            this.inventoryBluePot = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.playerHitPoints = new System.Windows.Forms.Label();
            this.batHitPoints = new System.Windows.Forms.Label();
            this.ghostHitPoints = new System.Windows.Forms.Label();
            this.ghoulHitPoints = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.moveLeft = new System.Windows.Forms.Button();
            this.moveRight = new System.Windows.Forms.Button();
            this.moveDown = new System.Windows.Forms.Button();
            this.moveUp = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.attackLeft = new System.Windows.Forms.Button();
            this.attackRight = new System.Windows.Forms.Button();
            this.attackDown = new System.Windows.Forms.Button();
            this.attackUp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.player)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghoul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redPotPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bluePotPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.swordPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bowPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.macePic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventorySword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryBow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryMorgensztern)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryRedPot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryBluePot)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // player
            // 
            this.player.BackColor = System.Drawing.Color.Transparent;
            this.player.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("player.BackgroundImage")));
            this.player.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.player.Location = new System.Drawing.Point(146, 97);
            this.player.Name = "player";
            this.player.Size = new System.Drawing.Size(50, 50);
            this.player.TabIndex = 0;
            this.player.TabStop = false;
            // 
            // bat
            // 
            this.bat.BackColor = System.Drawing.Color.Transparent;
            this.bat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bat.BackgroundImage")));
            this.bat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bat.Location = new System.Drawing.Point(202, 97);
            this.bat.Name = "bat";
            this.bat.Size = new System.Drawing.Size(50, 50);
            this.bat.TabIndex = 1;
            this.bat.TabStop = false;
            // 
            // ghost
            // 
            this.ghost.BackColor = System.Drawing.Color.Transparent;
            this.ghost.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ghost.BackgroundImage")));
            this.ghost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ghost.Location = new System.Drawing.Point(258, 97);
            this.ghost.Name = "ghost";
            this.ghost.Size = new System.Drawing.Size(50, 50);
            this.ghost.TabIndex = 2;
            this.ghost.TabStop = false;
            // 
            // ghoul
            // 
            this.ghoul.BackColor = System.Drawing.Color.Transparent;
            this.ghoul.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ghoul.BackgroundImage")));
            this.ghoul.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ghoul.Location = new System.Drawing.Point(314, 97);
            this.ghoul.Name = "ghoul";
            this.ghoul.Size = new System.Drawing.Size(50, 50);
            this.ghoul.TabIndex = 3;
            this.ghoul.TabStop = false;
            // 
            // redPotPic
            // 
            this.redPotPic.BackColor = System.Drawing.Color.Transparent;
            this.redPotPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("redPotPic.BackgroundImage")));
            this.redPotPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.redPotPic.Location = new System.Drawing.Point(370, 97);
            this.redPotPic.Name = "redPotPic";
            this.redPotPic.Size = new System.Drawing.Size(50, 50);
            this.redPotPic.TabIndex = 4;
            this.redPotPic.TabStop = false;
            // 
            // bluePotPic
            // 
            this.bluePotPic.BackColor = System.Drawing.Color.Transparent;
            this.bluePotPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bluePotPic.BackgroundImage")));
            this.bluePotPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bluePotPic.Location = new System.Drawing.Point(426, 97);
            this.bluePotPic.Name = "bluePotPic";
            this.bluePotPic.Size = new System.Drawing.Size(50, 50);
            this.bluePotPic.TabIndex = 5;
            this.bluePotPic.TabStop = false;
            // 
            // swordPic
            // 
            this.swordPic.BackColor = System.Drawing.Color.Transparent;
            this.swordPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("swordPic.BackgroundImage")));
            this.swordPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.swordPic.Location = new System.Drawing.Point(482, 97);
            this.swordPic.Name = "swordPic";
            this.swordPic.Size = new System.Drawing.Size(50, 50);
            this.swordPic.TabIndex = 6;
            this.swordPic.TabStop = false;
            // 
            // bowPic
            // 
            this.bowPic.BackColor = System.Drawing.Color.Transparent;
            this.bowPic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bowPic.BackgroundImage")));
            this.bowPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bowPic.Location = new System.Drawing.Point(538, 97);
            this.bowPic.Name = "bowPic";
            this.bowPic.Size = new System.Drawing.Size(50, 50);
            this.bowPic.TabIndex = 7;
            this.bowPic.TabStop = false;
            // 
            // macePic
            // 
            this.macePic.BackColor = System.Drawing.Color.Transparent;
            this.macePic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("macePic.BackgroundImage")));
            this.macePic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.macePic.Location = new System.Drawing.Point(594, 97);
            this.macePic.Name = "macePic";
            this.macePic.Size = new System.Drawing.Size(50, 50);
            this.macePic.TabIndex = 7;
            this.macePic.TabStop = false;
            // 
            // inventorySword
            // 
            this.inventorySword.BackColor = System.Drawing.Color.Transparent;
            this.inventorySword.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("inventorySword.BackgroundImage")));
            this.inventorySword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.inventorySword.Location = new System.Drawing.Point(131, 498);
            this.inventorySword.Name = "inventorySword";
            this.inventorySword.Size = new System.Drawing.Size(75, 75);
            this.inventorySword.TabIndex = 8;
            this.inventorySword.TabStop = false;
            this.inventorySword.Click += new System.EventHandler(this.inventorySword_Click);
            // 
            // inventoryBow
            // 
            this.inventoryBow.BackColor = System.Drawing.Color.Transparent;
            this.inventoryBow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("inventoryBow.BackgroundImage")));
            this.inventoryBow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.inventoryBow.Location = new System.Drawing.Point(212, 498);
            this.inventoryBow.Name = "inventoryBow";
            this.inventoryBow.Size = new System.Drawing.Size(75, 75);
            this.inventoryBow.TabIndex = 9;
            this.inventoryBow.TabStop = false;
            this.inventoryBow.Click += new System.EventHandler(this.inventoryBow_Click);
            // 
            // inventoryMorgensztern
            // 
            this.inventoryMorgensztern.BackColor = System.Drawing.Color.Transparent;
            this.inventoryMorgensztern.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("inventoryMorgensztern.BackgroundImage")));
            this.inventoryMorgensztern.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.inventoryMorgensztern.Location = new System.Drawing.Point(293, 498);
            this.inventoryMorgensztern.Name = "inventoryMorgensztern";
            this.inventoryMorgensztern.Size = new System.Drawing.Size(75, 75);
            this.inventoryMorgensztern.TabIndex = 10;
            this.inventoryMorgensztern.TabStop = false;
            this.inventoryMorgensztern.Click += new System.EventHandler(this.inventoryMorgensztern_Click);
            // 
            // inventoryRedPot
            // 
            this.inventoryRedPot.BackColor = System.Drawing.Color.Transparent;
            this.inventoryRedPot.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("inventoryRedPot.BackgroundImage")));
            this.inventoryRedPot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.inventoryRedPot.Location = new System.Drawing.Point(374, 498);
            this.inventoryRedPot.Name = "inventoryRedPot";
            this.inventoryRedPot.Size = new System.Drawing.Size(75, 75);
            this.inventoryRedPot.TabIndex = 11;
            this.inventoryRedPot.TabStop = false;
            this.inventoryRedPot.Click += new System.EventHandler(this.inventoryRedPot_Click);
            // 
            // inventoryBluePot
            // 
            this.inventoryBluePot.BackColor = System.Drawing.Color.Transparent;
            this.inventoryBluePot.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("inventoryBluePot.BackgroundImage")));
            this.inventoryBluePot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.inventoryBluePot.Location = new System.Drawing.Point(454, 498);
            this.inventoryBluePot.Name = "inventoryBluePot";
            this.inventoryBluePot.Size = new System.Drawing.Size(75, 75);
            this.inventoryBluePot.TabIndex = 12;
            this.inventoryBluePot.TabStop = false;
            this.inventoryBluePot.Click += new System.EventHandler(this.inventoryBluePot_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.46575F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.53425F));
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.playerHitPoints, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.batHitPoints, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ghostHitPoints, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.ghoulHitPoints, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(682, 385);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(195, 95);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(3, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Upiór";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Gracz";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nietoperz";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Duch";
            // 
            // playerHitPoints
            // 
            this.playerHitPoints.AutoSize = true;
            this.playerHitPoints.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.playerHitPoints.Location = new System.Drawing.Point(85, 0);
            this.playerHitPoints.Name = "playerHitPoints";
            this.playerHitPoints.Size = new System.Drawing.Size(0, 17);
            this.playerHitPoints.TabIndex = 4;
            // 
            // batHitPoints
            // 
            this.batHitPoints.AutoSize = true;
            this.batHitPoints.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.batHitPoints.Location = new System.Drawing.Point(85, 23);
            this.batHitPoints.Name = "batHitPoints";
            this.batHitPoints.Size = new System.Drawing.Size(0, 17);
            this.batHitPoints.TabIndex = 3;
            // 
            // ghostHitPoints
            // 
            this.ghostHitPoints.AutoSize = true;
            this.ghostHitPoints.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ghostHitPoints.Location = new System.Drawing.Point(85, 46);
            this.ghostHitPoints.Name = "ghostHitPoints";
            this.ghostHitPoints.Size = new System.Drawing.Size(0, 17);
            this.ghostHitPoints.TabIndex = 5;
            // 
            // ghoulHitPoints
            // 
            this.ghoulHitPoints.AutoSize = true;
            this.ghoulHitPoints.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ghoulHitPoints.Location = new System.Drawing.Point(85, 69);
            this.ghoulHitPoints.Name = "ghoulHitPoints";
            this.ghoulHitPoints.Size = new System.Drawing.Size(0, 17);
            this.ghoulHitPoints.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.moveLeft);
            this.groupBox1.Controls.Add(this.moveRight);
            this.groupBox1.Controls.Add(this.moveDown);
            this.groupBox1.Controls.Add(this.moveUp);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(551, 486);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 113);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "RUCH";
            // 
            // moveLeft
            // 
            this.moveLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.moveLeft.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveLeft.Location = new System.Drawing.Point(22, 43);
            this.moveLeft.Name = "moveLeft";
            this.moveLeft.Size = new System.Drawing.Size(46, 44);
            this.moveLeft.TabIndex = 3;
            this.moveLeft.Text = "◄";
            this.moveLeft.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.moveLeft.UseVisualStyleBackColor = true;
            this.moveLeft.Click += new System.EventHandler(this.moveLeft_Click);
            // 
            // moveRight
            // 
            this.moveRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.moveRight.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveRight.Location = new System.Drawing.Point(126, 43);
            this.moveRight.Name = "moveRight";
            this.moveRight.Size = new System.Drawing.Size(46, 44);
            this.moveRight.TabIndex = 2;
            this.moveRight.Text = "►";
            this.moveRight.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.moveRight.UseVisualStyleBackColor = true;
            this.moveRight.Click += new System.EventHandler(this.moveRight_Click);
            // 
            // moveDown
            // 
            this.moveDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.moveDown.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveDown.Location = new System.Drawing.Point(74, 69);
            this.moveDown.Name = "moveDown";
            this.moveDown.Size = new System.Drawing.Size(46, 44);
            this.moveDown.TabIndex = 1;
            this.moveDown.Text = "▼";
            this.moveDown.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.moveDown.UseVisualStyleBackColor = true;
            this.moveDown.Click += new System.EventHandler(this.moveDown_Click);
            // 
            // moveUp
            // 
            this.moveUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.moveUp.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveUp.Location = new System.Drawing.Point(74, 0);
            this.moveUp.Name = "moveUp";
            this.moveUp.Size = new System.Drawing.Size(46, 44);
            this.moveUp.TabIndex = 0;
            this.moveUp.Text = "▲";
            this.moveUp.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.moveUp.UseVisualStyleBackColor = true;
            this.moveUp.Click += new System.EventHandler(this.moveUp_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.attackLeft);
            this.groupBox2.Controls.Add(this.attackRight);
            this.groupBox2.Controls.Add(this.attackDown);
            this.groupBox2.Controls.Add(this.attackUp);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(757, 486);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 113);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "WALKA";
            // 
            // attackLeft
            // 
            this.attackLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.attackLeft.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.attackLeft.Location = new System.Drawing.Point(22, 43);
            this.attackLeft.Name = "attackLeft";
            this.attackLeft.Size = new System.Drawing.Size(46, 44);
            this.attackLeft.TabIndex = 3;
            this.attackLeft.Text = "◄";
            this.attackLeft.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.attackLeft.UseVisualStyleBackColor = true;
            this.attackLeft.Click += new System.EventHandler(this.attackLeft_Click);
            // 
            // attackRight
            // 
            this.attackRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.attackRight.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.attackRight.Location = new System.Drawing.Point(126, 43);
            this.attackRight.Name = "attackRight";
            this.attackRight.Size = new System.Drawing.Size(46, 44);
            this.attackRight.TabIndex = 2;
            this.attackRight.Text = "►";
            this.attackRight.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.attackRight.UseVisualStyleBackColor = true;
            this.attackRight.Click += new System.EventHandler(this.attackRight_Click);
            // 
            // attackDown
            // 
            this.attackDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.attackDown.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.attackDown.Location = new System.Drawing.Point(74, 69);
            this.attackDown.Name = "attackDown";
            this.attackDown.Size = new System.Drawing.Size(46, 44);
            this.attackDown.TabIndex = 1;
            this.attackDown.Text = "▼";
            this.attackDown.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.attackDown.UseVisualStyleBackColor = true;
            this.attackDown.Click += new System.EventHandler(this.attackDown_Click);
            // 
            // attackUp
            // 
            this.attackUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.attackUp.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.attackUp.Location = new System.Drawing.Point(74, 0);
            this.attackUp.Name = "attackUp";
            this.attackUp.Size = new System.Drawing.Size(46, 44);
            this.attackUp.TabIndex = 0;
            this.attackUp.Text = "▲";
            this.attackUp.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.attackUp.UseVisualStyleBackColor = true;
            this.attackUp.Click += new System.EventHandler(this.attackUp_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1066, 630);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.player);
            this.Controls.Add(this.inventoryBluePot);
            this.Controls.Add(this.inventoryRedPot);
            this.Controls.Add(this.inventoryMorgensztern);
            this.Controls.Add(this.inventoryBow);
            this.Controls.Add(this.inventorySword);
            this.Controls.Add(this.ghoul);
            this.Controls.Add(this.ghost);
            this.Controls.Add(this.bat);
            this.Controls.Add(this.macePic);
            this.Controls.Add(this.bowPic);
            this.Controls.Add(this.swordPic);
            this.Controls.Add(this.bluePotPic);
            this.Controls.Add(this.redPotPic);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Wyprawa";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.player)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghoul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redPotPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bluePotPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.swordPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bowPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.macePic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventorySword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryBow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryMorgensztern)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryRedPot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventoryBluePot)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox player;
        private System.Windows.Forms.PictureBox bat;
        private System.Windows.Forms.PictureBox ghost;
        private System.Windows.Forms.PictureBox ghoul;
        private System.Windows.Forms.PictureBox redPotPic;
        private System.Windows.Forms.PictureBox bluePotPic;
        private System.Windows.Forms.PictureBox swordPic;
        private System.Windows.Forms.PictureBox bowPic;
        private System.Windows.Forms.PictureBox macePic;
        private System.Windows.Forms.PictureBox inventorySword;
        private System.Windows.Forms.PictureBox inventoryBow;
        private System.Windows.Forms.PictureBox inventoryMorgensztern;
        private System.Windows.Forms.PictureBox inventoryRedPot;
        private System.Windows.Forms.PictureBox inventoryBluePot;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label playerHitPoints;
        private System.Windows.Forms.Label batHitPoints;
        private System.Windows.Forms.Label ghostHitPoints;
        private System.Windows.Forms.Label ghoulHitPoints;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button moveUp;
        private System.Windows.Forms.Button moveLeft;
        private System.Windows.Forms.Button moveRight;
        private System.Windows.Forms.Button moveDown;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button attackLeft;
        private System.Windows.Forms.Button attackRight;
        private System.Windows.Forms.Button attackDown;
        private System.Windows.Forms.Button attackUp;
    }
}

