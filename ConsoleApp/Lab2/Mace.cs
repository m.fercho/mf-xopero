﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab2
{
    class Mace : Weapon
    {
        private int MaceRadius = 20;
        private int MaceDamage = 6;
        public Mace(Game game, Point location) : base (game, location) { }
        public override string Name { get { return "Morgensztern"; } }
        public override void Attack(Direction direction, Random random)
        {
            if (!DamageEnemy(direction, MaceRadius, MaceDamage, random))
            {
                if (!DamageEnemy(RightDirection(direction), MaceRadius, MaceDamage, random))
                {
                    DamageEnemy(LeftDirection(direction), MaceRadius, MaceDamage, random);
                }
            }
        }
    }
}
