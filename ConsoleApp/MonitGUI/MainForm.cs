﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Dynamic;
using System.Management;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.ServiceProcess;

namespace MonitGUI
{
    public partial class MainForm : Form
    {
        NotepadForm notepadForm;
        DataBrowserForm dataBrowserForm;
        ByteToReadableValue byteToValue;
        ProcessStartInfo processStartInfo;
        ServiceController serviceController;
        private string query, status, 
            keyLoggerPath = @"C:\Users\mat3u\Documents\xopero\ConsoleApp\KeyLogger\bin\Debug\KeyLogger.exe", 
            howOftenPath = @"C:\Users\mat3u\Documents\xopero\serviceSupportFolder\HowOftenFile.txt";
        private int HowOften;
        dynamic response, extraProcessInfo;
        public MainForm()
        {
            InitializeComponent();
            byteToValue = new ByteToReadableValue();
            HowOften = (int)HowOftenNumericUpDown.Value;
         //   ServiceStart("Service1", HowOften);
        }
        // Metoda do uruchamiania usługi
        private void ServiceStart(string serviceName, int howOften)
        {
            serviceController = new ServiceController(serviceName);
            if ((serviceController.Status.Equals(ServiceControllerStatus.Stopped)) ||
     (serviceController.Status.Equals(ServiceControllerStatus.StopPending)))
            {
                serviceController.Start();
            }
        }
        // Metoda do zatrymywania usługi
        private void ServiceStop()
        {
                serviceController.Stop();
        }
        // Obsługa keyloggera
        private void KeyLog()
        {
            processStartInfo = new ProcessStartInfo(keyLoggerPath);
            if (KeyloggerStartCheckbox.Checked)
            {
                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                Process.Start(processStartInfo);
            }
            else
            {
                foreach (Process keyLoggerProcess in Process.GetProcessesByName("KeyLogger"))
                {
                    keyLoggerProcess.Kill();
                }
            }
        }
        // Informacje o procesach
        public ExpandoObject GetProcessExtraInformation(int processId)
        {
            // Zapytanie do Win32_Process
            query = "SELECT * FROM Win32_Process WHERE ProcessID = " + processId;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection processList = searcher.Get();
            // Tworzenie dynamicznego objektu do przechowywania informacji
            response = new ExpandoObject();
            response.Description = "";
            response.Username = "Nieznany";
            foreach (ManagementObject obj in processList)
            {
                // Zwracanie użytkownika 
                string[] argList = new string[] { string.Empty, string.Empty };
                int returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                if (returnVal == 0)
                {
                    response.Username = argList[0];
                }
                // Zwraca opis procesu - jesli istnieje
                if (obj["ExecutablePath"] != null)
                {
                    try
                    {
                        FileVersionInfo info = FileVersionInfo.GetVersionInfo(obj["ExecutablePath"].ToString());
                        response.Description = info.FileDescription;
                    }
                    catch { }
                }
            }
            return response;
        }
        // Metoda wypisująca elementy do listView
        public void renderProcessesOnListView()
        {
            // Tworzenie tablicy przechowującej procesy
            Process[] processList = Process.GetProcesses();
            // Pętla po tablicy wyświetlająca informacje o procesach
            foreach (Process process in processList)
            {
                // Zamiana wartości statusu z bolean na string
                status = (process.Responding == true ? "Żyje" : "Nie odpowiada");
                // Ekstra informacje do opisu i użytkownika
                extraProcessInfo = GetProcessExtraInformation(process.Id);
                // Tablica przechowująca informacje do wyśeitlenia
                string[] row =
                    {
                    // Nazwa procesu: 
                    process.ProcessName,
                    // PID: 
                    process.Id.ToString(),
                    // Status: 
                    status,
                    // nazwa użytkownika: 
                    extraProcessInfo.Username,
                    // Pamięć: 
                    byteToValue.BytesToReadableValue(process.PrivateMemorySize64),
                    // Opis: 
                    extraProcessInfo.Description
                    };
                ListViewItem item = new ListViewItem(row);
                ProcessesListView.Items.Add(item);
            }
        }
        // Przyciski i inne kontrolki formularza
        // Przycisk włączania przeglądarki historycznych danych
        private void DataBrowserButton_Click(object sender, EventArgs e)
        {
            dataBrowserForm = new DataBrowserForm();
            dataBrowserForm.Show();
        }
        // Wyłaczanie usługi
        private void StopServiceButton_Click(object sender, EventArgs e)
        {
            ServiceStop();
        }
        // Ponowne uruchamianie usługi
        private void StartServiceButton_Click(object sender, EventArgs e)
        {
            ServiceStart("Service1", HowOften);
        }
        // Przycisk włączania notatnika
        private void NotepadButton_Click(object sender, EventArgs e)
        {
            notepadForm = new NotepadForm();
            notepadForm.Show();
        }
        // Przycisk odżieżania listy procesów
        private void RefreshButton_Click(object sender, EventArgs e)
        {
            renderProcessesOnListView();
        }
        // Checkbox do uruchamiania Keyloggera
        private void KeyloggerStartCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            KeyLog();
        }
        // Zwracanie wartości NumericUpDown tuż po zmianie wartości
        private void HowOftenNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            HowOften = (int)HowOftenNumericUpDown.Value;
            using (StreamWriter sw = new StreamWriter(howOftenPath))
            {
                sw.WriteLine(HowOften);
            }
        }
    }
}
