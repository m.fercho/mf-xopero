﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MonitGUI
{
    public partial class DataBrowserForm : Form
    {
        DataBrowserForm dataBrowserForm;
        DBOperations dbOperations;
        private string id, cpuSqlQuerry, ramSqlQuerry, processesSqlQuerry, idSqlQuerry, date, time, dbPath = @"C:\Users\mat3u\Documents\xopero\monitor_DB\monDB.db",
            FillTimeComboBoxSqlQuerry;
        public DataBrowserForm()
        {
            InitializeComponent();
            dbOperations = new DBOperations(dbPath);
            DatePicker.Format = DateTimePickerFormat.Custom;
            DatePicker.CustomFormat = "yyyy-MM-dd";
        }
        // Otwieranie nowego okna z menuStrip
        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataBrowserForm = new DataBrowserForm();
            dataBrowserForm.Show();
        }
        // Operacje z datą
        private void DatePicker_ValueChanged(object sender, EventArgs e)
        {
            TimeComboBox.Items.Clear();
            date = DatePicker.Text;
            FillTimeComboBoxSqlQuerry = "SELECT Time "
                        + "FROM raportTable "
                        + "WHERE Date = '"
                        + date
                        + "'";
            dbOperations.DBFillTimeComboBox(dbPath, FillTimeComboBoxSqlQuerry);
            /*
            // Inna metoda wyświetlania danych w comboBox
            TimeComboBox.ValueMember = "RaportID".ToString();
            TimeComboBox.DisplayMember = "ID".ToString().Trim();
            TimeComboBox.DisplayMember = "Date";
            TimeComboBox.DataSource = dbOperations.myDataTable;
            */
            while (dbOperations.reader.Read())
            {
                TimeComboBox.Items.Add(dbOperations.reader.GetString(0));
            }
        }
        // Operacje z czasem (wyświetlanie dataGridView)
        private void TimeComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            time = TimeComboBox.Text;
            idSqlQuerry = "SELECT ID "
                        + "FROM raportTable "
                        + "WHERE Time = '"
                        + time
                        + "'";
            dbOperations.DBQuerry(idSqlQuerry);
            id = dbOperations.sID;
            DBSearchAndFill(CpuDataGrid, RamDataGrid, ProcessDataGrid);
        }
        // Tworzenie zapytań i wywołanie metody wypełniającej dataGridView
        private void DBSearchAndFill(DataGridView cpuDataGrid, DataGridView ramDataGrid2, DataGridView processDataGrid)
        {
            cpuSqlQuerry = "SELECT * "
                        + "FROM cpuTable "
                        + "WHERE RaportID = '"
                        + id
                        + "'";
            ramSqlQuerry = "SELECT * "
                        + "FROM ramTable "
                        + "WHERE RaportID = '"
                        + id
                        + "'";
            processesSqlQuerry = "SELECT * "
                        + "FROM processTable "
                        + "WHERE RaportID = '"
                        + id
                        + "'";
            FillDataGridView(cpuSqlQuerry, cpuDataGrid);
            FillDataGridView(ramSqlQuerry, ramDataGrid2);
            FillDataGridView(processesSqlQuerry, processDataGrid);
        } 
        // Metoda wypełniająca dataGridView
        private void FillDataGridView(string querry, DataGridView dataGrid)
        {
            dbOperations.DBConnection(dbPath);
            dbOperations.dataSet = new DataSet();
            dbOperations.dataAdapter =
                new SQLiteDataAdapter(querry, dbOperations.dbConnect);
            dbOperations.dataAdapter.Fill(dbOperations.dataSet);
            dataGrid.DataSource = dbOperations.dataSet.Tables[0].DefaultView;
        }
    }
}
