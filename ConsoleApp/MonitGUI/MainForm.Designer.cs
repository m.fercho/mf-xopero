﻿namespace MonitGUI
{
    partial class MainForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.ProcessesListView = new System.Windows.Forms.ListView();
            this.ProcessNameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PIDHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.StatusHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.UserHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MemoryHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DescriptionHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.HowOftenNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.MemoryUsedRAMLabel = new System.Windows.Forms.Label();
            this.CoreUsedCPULabel = new System.Windows.Forms.Label();
            this.PercentRAMLabel = new System.Windows.Forms.Label();
            this.PercentCPULabel = new System.Windows.Forms.Label();
            this.ProgressBarCPU = new System.Windows.Forms.ProgressBar();
            this.ProgressBarRAM = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DataBrowserButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NotepadButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.KeyloggerStartCheckbox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.StopServiceButton = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.StartServiceButton = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HowOftenNumericUpDown)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(657, 578);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.RefreshButton);
            this.tabPage1.Controls.Add(this.ProcessesListView);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(649, 541);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Procesy";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(8, 489);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(156, 44);
            this.RefreshButton.TabIndex = 2;
            this.RefreshButton.Text = "Odśwież";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // ProcessesListView
            // 
            this.ProcessesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ProcessNameHeader,
            this.PIDHeader,
            this.StatusHeader,
            this.UserHeader,
            this.MemoryHeader,
            this.DescriptionHeader});
            this.ProcessesListView.Dock = System.Windows.Forms.DockStyle.Top;
            this.ProcessesListView.Location = new System.Drawing.Point(3, 3);
            this.ProcessesListView.Name = "ProcessesListView";
            this.ProcessesListView.Size = new System.Drawing.Size(643, 480);
            this.ProcessesListView.TabIndex = 0;
            this.ProcessesListView.UseCompatibleStateImageBehavior = false;
            this.ProcessesListView.View = System.Windows.Forms.View.Details;
            // 
            // ProcessNameHeader
            // 
            this.ProcessNameHeader.Text = "Nazwa procesu";
            this.ProcessNameHeader.Width = 140;
            // 
            // PIDHeader
            // 
            this.PIDHeader.Text = "PID";
            this.PIDHeader.Width = 50;
            // 
            // StatusHeader
            // 
            this.StatusHeader.Text = "Status";
            this.StatusHeader.Width = 100;
            // 
            // UserHeader
            // 
            this.UserHeader.Text = "Użytkownik";
            this.UserHeader.Width = 120;
            // 
            // MemoryHeader
            // 
            this.MemoryHeader.Text = "Pamięć";
            this.MemoryHeader.Width = 100;
            // 
            // DescriptionHeader
            // 
            this.DescriptionHeader.Text = "Opis";
            this.DescriptionHeader.Width = 100;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(649, 541);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Wydajność";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.MemoryUsedRAMLabel);
            this.groupBox1.Controls.Add(this.CoreUsedCPULabel);
            this.groupBox1.Controls.Add(this.PercentRAMLabel);
            this.groupBox1.Controls.Add(this.PercentCPULabel);
            this.groupBox1.Controls.Add(this.ProgressBarCPU);
            this.groupBox1.Controls.Add(this.ProgressBarRAM);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(643, 535);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Monitor zasobów";
            // 
            // HowOftenNumericUpDown
            // 
            this.HowOftenNumericUpDown.Location = new System.Drawing.Point(281, 40);
            this.HowOftenNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.HowOftenNumericUpDown.Name = "HowOftenNumericUpDown";
            this.HowOftenNumericUpDown.Size = new System.Drawing.Size(86, 32);
            this.HowOftenNumericUpDown.TabIndex = 9;
            this.HowOftenNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.HowOftenNumericUpDown.ValueChanged += new System.EventHandler(this.HowOftenNumericUpDown_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(143, 24);
            this.label6.TabIndex = 8;
            this.label6.Text = "Zbieraj dane co:";
            // 
            // MemoryUsedRAMLabel
            // 
            this.MemoryUsedRAMLabel.AutoSize = true;
            this.MemoryUsedRAMLabel.Location = new System.Drawing.Point(467, 70);
            this.MemoryUsedRAMLabel.Name = "MemoryUsedRAMLabel";
            this.MemoryUsedRAMLabel.Size = new System.Drawing.Size(116, 24);
            this.MemoryUsedRAMLabel.TabIndex = 7;
            this.MemoryUsedRAMLabel.Text = "nnnn / nnnn";
            // 
            // CoreUsedCPULabel
            // 
            this.CoreUsedCPULabel.AutoSize = true;
            this.CoreUsedCPULabel.Location = new System.Drawing.Point(467, 44);
            this.CoreUsedCPULabel.Name = "CoreUsedCPULabel";
            this.CoreUsedCPULabel.Size = new System.Drawing.Size(116, 24);
            this.CoreUsedCPULabel.TabIndex = 6;
            this.CoreUsedCPULabel.Text = "nnnn / nnnn";
            // 
            // PercentRAMLabel
            // 
            this.PercentRAMLabel.AutoSize = true;
            this.PercentRAMLabel.Location = new System.Drawing.Point(379, 70);
            this.PercentRAMLabel.Name = "PercentRAMLabel";
            this.PercentRAMLabel.Size = new System.Drawing.Size(24, 24);
            this.PercentRAMLabel.TabIndex = 5;
            this.PercentRAMLabel.Text = "%";
            // 
            // PercentCPULabel
            // 
            this.PercentCPULabel.AutoSize = true;
            this.PercentCPULabel.Location = new System.Drawing.Point(379, 44);
            this.PercentCPULabel.Name = "PercentCPULabel";
            this.PercentCPULabel.Size = new System.Drawing.Size(24, 24);
            this.PercentCPULabel.TabIndex = 4;
            this.PercentCPULabel.Text = "%";
            // 
            // ProgressBarCPU
            // 
            this.ProgressBarCPU.Location = new System.Drawing.Point(130, 44);
            this.ProgressBarCPU.Name = "ProgressBarCPU";
            this.ProgressBarCPU.Size = new System.Drawing.Size(243, 23);
            this.ProgressBarCPU.TabIndex = 3;
            // 
            // ProgressBarRAM
            // 
            this.ProgressBarRAM.Location = new System.Drawing.Point(130, 70);
            this.ProgressBarRAM.Name = "ProgressBarRAM";
            this.ProgressBarRAM.Size = new System.Drawing.Size(243, 23);
            this.ProgressBarRAM.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pamięć RAM";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Procesor";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 33);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(649, 541);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Dodatkowe opcje";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.DataBrowserButton);
            this.groupBox4.Location = new System.Drawing.Point(10, 267);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(631, 133);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data browser";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(506, 24);
            this.label5.TabIndex = 1;
            this.label5.Text = "Przeglądanie zebranych wczesniej danych diagnostycznych. ";
            // 
            // DataBrowserButton
            // 
            this.DataBrowserButton.Location = new System.Drawing.Point(14, 64);
            this.DataBrowserButton.Name = "DataBrowserButton";
            this.DataBrowserButton.Size = new System.Drawing.Size(157, 47);
            this.DataBrowserButton.TabIndex = 2;
            this.DataBrowserButton.Text = "Data browser";
            this.DataBrowserButton.UseVisualStyleBackColor = true;
            this.DataBrowserButton.Click += new System.EventHandler(this.DataBrowserButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.NotepadButton);
            this.groupBox3.Location = new System.Drawing.Point(10, 128);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(631, 133);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Notatnik";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(267, 24);
            this.label4.TabIndex = 1;
            this.label4.Text = "Zapis do pliku / odczyt z pliku. ";
            // 
            // NotepadButton
            // 
            this.NotepadButton.Location = new System.Drawing.Point(14, 65);
            this.NotepadButton.Name = "NotepadButton";
            this.NotepadButton.Size = new System.Drawing.Size(157, 47);
            this.NotepadButton.TabIndex = 1;
            this.NotepadButton.Text = "Notatnik";
            this.NotepadButton.UseVisualStyleBackColor = true;
            this.NotepadButton.Click += new System.EventHandler(this.NotepadButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.KeyloggerStartCheckbox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(10, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(631, 116);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rejestrator wciskanych klawiszy";
            // 
            // KeyloggerStartCheckbox
            // 
            this.KeyloggerStartCheckbox.AutoSize = true;
            this.KeyloggerStartCheckbox.Location = new System.Drawing.Point(14, 55);
            this.KeyloggerStartCheckbox.Name = "KeyloggerStartCheckbox";
            this.KeyloggerStartCheckbox.Size = new System.Drawing.Size(157, 28);
            this.KeyloggerStartCheckbox.TabIndex = 0;
            this.KeyloggerStartCheckbox.Text = "Start Keylogger";
            this.KeyloggerStartCheckbox.UseVisualStyleBackColor = true;
            this.KeyloggerStartCheckbox.CheckedChanged += new System.EventHandler(this.KeyloggerStartCheckbox_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(462, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Controls (On / Off) the keylogger part of the program. ";
            // 
            // StopServiceButton
            // 
            this.StopServiceButton.Location = new System.Drawing.Point(10, 78);
            this.StopServiceButton.Name = "StopServiceButton";
            this.StopServiceButton.Size = new System.Drawing.Size(258, 43);
            this.StopServiceButton.TabIndex = 10;
            this.StopServiceButton.Text = "Wyłącz usługę";
            this.StopServiceButton.UseVisualStyleBackColor = true;
            this.StopServiceButton.Click += new System.EventHandler(this.StopServiceButton_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.StartServiceButton);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.StopServiceButton);
            this.groupBox5.Controls.Add(this.HowOftenNumericUpDown);
            this.groupBox5.Location = new System.Drawing.Point(6, 108);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(631, 180);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Zbieranie danych";
            // 
            // StartServiceButton
            // 
            this.StartServiceButton.Location = new System.Drawing.Point(10, 127);
            this.StartServiceButton.Name = "StartServiceButton";
            this.StartServiceButton.Size = new System.Drawing.Size(258, 43);
            this.StartServiceButton.TabIndex = 11;
            this.StartServiceButton.Text = "Uruchom usługę ponownie";
            this.StartServiceButton.UseVisualStyleBackColor = true;
            this.StartServiceButton.Click += new System.EventHandler(this.StartServiceButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 578);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Panel kontrolny";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HowOftenNumericUpDown)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label MemoryUsedRAMLabel;
        private System.Windows.Forms.Label CoreUsedCPULabel;
        private System.Windows.Forms.Label PercentRAMLabel;
        private System.Windows.Forms.Label PercentCPULabel;
        private System.Windows.Forms.ProgressBar ProgressBarCPU;
        private System.Windows.Forms.ProgressBar ProgressBarRAM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox KeyloggerStartCheckbox;
        private System.Windows.Forms.ListView ProcessesListView;
        private System.Windows.Forms.ColumnHeader ProcessNameHeader;
        private System.Windows.Forms.ColumnHeader PIDHeader;
        private System.Windows.Forms.ColumnHeader StatusHeader;
        private System.Windows.Forms.ColumnHeader UserHeader;
        private System.Windows.Forms.ColumnHeader MemoryHeader;
        private System.Windows.Forms.ColumnHeader DescriptionHeader;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.Button DataBrowserButton;
        private System.Windows.Forms.Button NotepadButton;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown HowOftenNumericUpDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button StartServiceButton;
        private System.Windows.Forms.Button StopServiceButton;
    }
}

