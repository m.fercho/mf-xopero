﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitGUI
{
    class DBOperations
    {
        public SQLiteConnection dbConnect;
        public DataSet dataSet;
        public SQLiteDataAdapter dataAdapter;
        SQLiteCommand command;
    //    public DataTable dataTable;
        public SQLiteDataReader reader;
        private string DbPath { get; set; }
        public string sID;
        public DBOperations(string dbPath)
        {
            DbPath = dbPath;
        }
        // Łączenie się z bazą danych
        public void DBConnection(string DbPath)
        {
            dbConnect = new SQLiteConnection("Data Source=" + DbPath + ";Version=3;");
            dbConnect.Open();
        }
        // Metoda wykorzystywana przy zwracaniu id raportu na podstawie czasu wykonania
        public void DBQuerry(string sqlQuerry)
        {
            dbConnect = new SQLiteConnection("Data Source=" + DbPath + ";Version=3;");
            SQLiteCommand command = new SQLiteCommand(sqlQuerry, dbConnect);
            dbConnect.Open();
            sID = command.ExecuteScalar().ToString();
            dbConnect.Close();
        }
        // Metoda wykorzystywana przy wypełnianiu dataGridView
        public void DBFillTimeComboBox(string DbPath, string Querry)
        {
            // Metoda #1
            /*
            dbConnect = new SQLiteConnection("Data Source=" + DbPath + ";Version=3;");
            dbConnect.Open();
            command = new SQLiteCommand(Querry, dbConnect);
            reader = command.ExecuteReader();
            myDataTable = new MyDataTable();
            myDataTable.Columns.Add("Time").ToString();
            myDataTable.Load(reader);
            dbConnect.Close();
            */
            // Metoda #2
            DBConnection(DbPath);
            command = new SQLiteCommand(Querry, dbConnect);
            reader = command.ExecuteReader();
        }
    }
}
