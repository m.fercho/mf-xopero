﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz7_Str335
{
    class Bee : IStringPatrol
    {
        public bool LookForEnemies()
        {
            return true;
        }
        public int SharpenStinger(int Length)
        {
            return 0;
        }
        public int AlertLevel { get; }
        public int StingerLength { get; set; }
    }
}
