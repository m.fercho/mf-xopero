﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz8_Str409_PiecKart
{
    class Program
    {
        static void Main(string[] args)
        {
            Random myRandom = new Random();
            List<Card> cards = new List<Card>();
            Console.WriteLine("Pięć losowych kart:");
            for (int i = 0; i < 5; i++)
            {
                cards.Add(new Card((Suits)myRandom.Next(4),
                                   (Values)myRandom.Next(1, 14)));
                Console.WriteLine(cards[i].Name);
            }
            Console.WriteLine();

            cards.Sort(new CardComparer_byValue());
            foreach (Card card in cards)
            {
                Console.WriteLine(card.Name);
            }

            Console.ReadKey();
        }
    }
}
