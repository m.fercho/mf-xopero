﻿namespace Roz8_Str439_Lumberjack
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.addLumberjack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.line = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.nextLumberjack = new System.Windows.Forms.Button();
            this.addFlapjack = new System.Windows.Forms.Button();
            this.nextInLine = new System.Windows.Forms.RichTextBox();
            this.radioBanan = new System.Windows.Forms.RadioButton();
            this.radioBrowned = new System.Windows.Forms.RadioButton();
            this.radioSoggy = new System.Windows.Forms.RadioButton();
            this.radioCrispy = new System.Windows.Forms.RadioButton();
            this.howMany = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.howMany)).BeginInit();
            this.SuspendLayout();
            // 
            // addLumberjack
            // 
            this.addLumberjack.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.addLumberjack.Location = new System.Drawing.Point(9, 58);
            this.addLumberjack.Name = "addLumberjack";
            this.addLumberjack.Size = new System.Drawing.Size(311, 32);
            this.addLumberjack.TabIndex = 0;
            this.addLumberjack.Text = "Dodaj drwala";
            this.addLumberjack.UseVisualStyleBackColor = true;
            this.addLumberjack.Click += new System.EventHandler(this.addLumberjack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Imię drwala: ";
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.name.Location = new System.Drawing.Point(121, 19);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(199, 32);
            this.name.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.addLumberjack);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 102);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.line);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(12, 120);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(328, 503);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kolejka do śniadania";
            // 
            // line
            // 
            this.line.FormattingEnabled = true;
            this.line.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.line.ItemHeight = 24;
            this.line.Location = new System.Drawing.Point(10, 31);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(310, 460);
            this.line.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.nextLumberjack);
            this.groupBox3.Controls.Add(this.addFlapjack);
            this.groupBox3.Controls.Add(this.nextInLine);
            this.groupBox3.Controls.Add(this.radioBanan);
            this.groupBox3.Controls.Add(this.radioBrowned);
            this.groupBox3.Controls.Add(this.radioSoggy);
            this.groupBox3.Controls.Add(this.radioCrispy);
            this.groupBox3.Controls.Add(this.howMany);
            this.groupBox3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(346, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(349, 611);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Nakarm drwala";
            // 
            // nextLumberjack
            // 
            this.nextLumberjack.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nextLumberjack.Location = new System.Drawing.Point(15, 502);
            this.nextLumberjack.Name = "nextLumberjack";
            this.nextLumberjack.Size = new System.Drawing.Size(318, 32);
            this.nextLumberjack.TabIndex = 6;
            this.nextLumberjack.Text = "Następny!";
            this.nextLumberjack.UseVisualStyleBackColor = true;
            this.nextLumberjack.Click += new System.EventHandler(this.nextLumberjack_Click);
            // 
            // addFlapjack
            // 
            this.addFlapjack.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.addFlapjack.Location = new System.Drawing.Point(15, 262);
            this.addFlapjack.Name = "addFlapjack";
            this.addFlapjack.Size = new System.Drawing.Size(318, 32);
            this.addFlapjack.TabIndex = 3;
            this.addFlapjack.Text = "Dodaj naleśnik";
            this.addFlapjack.UseVisualStyleBackColor = true;
            this.addFlapjack.Click += new System.EventHandler(this.addFlapjack_Click);
            // 
            // nextInLine
            // 
            this.nextInLine.Enabled = false;
            this.nextInLine.Location = new System.Drawing.Point(15, 300);
            this.nextInLine.Name = "nextInLine";
            this.nextInLine.Size = new System.Drawing.Size(318, 196);
            this.nextInLine.TabIndex = 5;
            this.nextInLine.Text = "";
            // 
            // radioBanan
            // 
            this.radioBanan.AutoSize = true;
            this.radioBanan.Location = new System.Drawing.Point(15, 210);
            this.radioBanan.Name = "radioBanan";
            this.radioBanan.Size = new System.Drawing.Size(118, 28);
            this.radioBanan.TabIndex = 4;
            this.radioBanan.Text = "Bananowy";
            this.radioBanan.UseVisualStyleBackColor = true;
            // 
            // radioBrowned
            // 
            this.radioBrowned.AutoSize = true;
            this.radioBrowned.Location = new System.Drawing.Point(15, 176);
            this.radioBrowned.Name = "radioBrowned";
            this.radioBrowned.Size = new System.Drawing.Size(104, 28);
            this.radioBrowned.TabIndex = 3;
            this.radioBrowned.Text = "Rumiany";
            this.radioBrowned.UseVisualStyleBackColor = true;
            // 
            // radioSoggy
            // 
            this.radioSoggy.AutoSize = true;
            this.radioSoggy.Location = new System.Drawing.Point(15, 142);
            this.radioSoggy.Name = "radioSoggy";
            this.radioSoggy.Size = new System.Drawing.Size(106, 28);
            this.radioSoggy.TabIndex = 2;
            this.radioSoggy.Text = "Wilgotny";
            this.radioSoggy.UseVisualStyleBackColor = true;
            // 
            // radioCrispy
            // 
            this.radioCrispy.AutoSize = true;
            this.radioCrispy.Checked = true;
            this.radioCrispy.Location = new System.Drawing.Point(15, 108);
            this.radioCrispy.Name = "radioCrispy";
            this.radioCrispy.Size = new System.Drawing.Size(96, 28);
            this.radioCrispy.TabIndex = 1;
            this.radioCrispy.TabStop = true;
            this.radioCrispy.Text = "Chrupki";
            this.radioCrispy.UseVisualStyleBackColor = true;
            // 
            // howMany
            // 
            this.howMany.Location = new System.Drawing.Point(15, 58);
            this.howMany.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.howMany.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.howMany.Name = "howMany";
            this.howMany.Size = new System.Drawing.Size(120, 32);
            this.howMany.TabIndex = 0;
            this.howMany.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 635);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Calibri", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Śniadanie dla drwali";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.howMany)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addLumberjack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox line;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button nextLumberjack;
        private System.Windows.Forms.Button addFlapjack;
        private System.Windows.Forms.RichTextBox nextInLine;
        private System.Windows.Forms.RadioButton radioBanan;
        private System.Windows.Forms.RadioButton radioBrowned;
        private System.Windows.Forms.RadioButton radioSoggy;
        private System.Windows.Forms.RadioButton radioCrispy;
        private System.Windows.Forms.NumericUpDown howMany;
    }
}

