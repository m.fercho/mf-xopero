﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz8_Str439_Lumberjack
{
    public partial class Form1 : Form
    {
        private Queue<Lumberjack> breakfastLine = new Queue<Lumberjack>();
        public Form1()
        {
            InitializeComponent();
        }
        private void RedrawList()
        {
            int numberOfJack = 1;
            line.Items.Clear();
            foreach (Lumberjack lumber in breakfastLine)
            {
                line.Items.Add(numberOfJack + ". " + lumber.Name);
                numberOfJack++;
            }
            if (breakfastLine.Count == 0)
            {
                groupBox3.Enabled = false;
                nextInLine.Text = null;
            }
            else
            {
                groupBox3.Enabled = true;
                Lumberjack currentLumberjack = breakfastLine.Peek();
                nextInLine.Text = currentLumberjack.Name + " ma " + currentLumberjack.FlapjackCount 
                    + " naleśników.";
            }
        }

        private void addLumberjack_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(name.Text)) return;
            breakfastLine.Enqueue(new Lumberjack(name.Text));
            name.Text = null;
            RedrawList();
        }

        private void addFlapjack_Click(object sender, EventArgs e)
        {
            if (breakfastLine.Count == 0) return;
            Flapjack food;
            if (radioCrispy.Checked)
                food = Flapjack.Chrupkiego;
            else if (radioSoggy.Checked)
                food = Flapjack.Wilgotnego;
            else if (radioBrowned.Checked)
                food = Flapjack.Rumianego;
            else
                food = Flapjack.Bananowego;
            Lumberjack currentLumberjack = breakfastLine.Peek();
            currentLumberjack.TakeFlapjacks(food, (int)howMany.Value);
            RedrawList();
        }

        private void nextLumberjack_Click(object sender, EventArgs e)
        {
            if (breakfastLine.Count == 0) return;
            Lumberjack nextLumberjack = breakfastLine.Dequeue();
            nextLumberjack.EatFlapjacks();
            nextInLine.Text = null;
            RedrawList();
        }
    }
}
