﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using MonitDLL;

namespace TestWinForms
{
    public partial class Form1 : Form
    {
        Monitor monitor;
        private int HowOften;
        public Form1()
        {
            InitializeComponent();
            HowOften = (int)numericUpDown1.Value;
            monitor = new Monitor(HowOften);
        }
        // Przycisk do uruchamiania
        private void StartButton_Click(object sender, EventArgs e)
        {
            monitor.MonitorStart();
        }
        // PRzycisk do zatrzymania
        private void StopButton_Click(object sender, EventArgs e)
        {
            monitor.MonitorStop();
            Application.Exit();
        }
        //Przycisk testowy
        private void TestButton_Click(object sender, EventArgs e)
        {
            monitor.MonitorTest();
        }
        // Przycisk do jednokrotnego uruchomienia
        private void OneTimeButton_Click(object sender, EventArgs e)
        {
            monitor.MonitorOneTimeRund();
        }
    }
}
