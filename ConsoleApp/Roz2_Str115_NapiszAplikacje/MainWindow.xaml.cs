﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Roz2_Str115_NapiszAplikacje
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            string name = "Quentin";
            int x = 3;
            x = x * 17;
            double d = Math.PI / 2;
            myLabel.Text = "Imię to: " + name
                + "\n Wartość x to: " + x
                + "\n Wartość d to: " + d;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            //  int x = 5;
            int x = 10;
            if (x == 10)
            {
                myLabel.Text = "Wartość x musi byc równa 10";
            }
            else
            {
                myLabel.Text = "Wartość nie jest równa 5!";
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            int someValue = 4;
         //   string name = "Krzysiek";
            string name = "Janek";
            if ((someValue == 3) && (name == "Janek"))
            {
                myLabel.Text = "Wartość 'someValue' jest równa 3, a 'name' Janek";
            }
            myLabel.Text = "Ten wiersz jest wykonywany, bez względu na warunki.";
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            int count = 0;
            while (count < 10)
            {
                count = count + 1;
            }
            for (int i = 0; i < 5; i++)
            {
                count = count - 1;
            }
            myLabel.Text = "Odpowiedź to: " + count;
        }
    }
}
