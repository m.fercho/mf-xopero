﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz15_Str759_ConsoleAppDelegat
{
    class Program
    {
        static void Main(string[] args)
        {
            ConvertsIntToString someMethod = new ConvertsIntToString(HiThere);
            string message = someMethod(5);
            Console.WriteLine(message
                + " \r\n Naciśnij dowolny klawisz, aby zakończyć działanie programu.");
            Console.ReadKey();
        }
        private static string HiThere(int i)
        {
            return "Witajcie, towarzyszu numer: " + (i * 100);
        }
    }
}
