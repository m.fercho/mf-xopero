﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace BookCatalog
{
    public partial class AddForm : Form
    {
        public SQLiteCommand command;
        DBOperations dbOperations;
        private string sqlQuerryInsert, titleSqlQuerry, authorSqlQuerry, genreSqlQuerry, printSqlQuerry, typeSqlQuerry, descSqlQuerry, historySqlQuerry, checkType, checkGenre, errorBoxShow;
        private int yearSqlQuerry, yearIntToNull;
        public AddForm(DBOperations dbOperations)
        {
            InitializeComponent();
            this.dbOperations = dbOperations;
            errorBoxShow = null;
        }
        // Metoda zwracająca wartość - typ ksiazki
        private string GetTextFromRadioButtons()
        {
            if (OrdinaryBookRadio.Checked)
                checkType = "Ksiązka zwyczajna";
            if (LectureBookRadio.Checked)
                checkType = "Lektura";
            if (WhiteCrowRadio.Checked)
                checkType = "Biały kruk";
            return checkType;
        }
        // Metoda zwracająca wartość - rok szkolny
        private int GetTextFromComboBox()
        {
            if (yearComboBox.Text == "1")
                yearIntToNull = 1;
            if (yearComboBox.Text == "2")
                yearIntToNull = 2;
            if (yearComboBox.Text == "3")
                yearIntToNull = 3;
            return yearIntToNull;
        }
        // Metoda sprawdzająca radioButtony - typ ksiązki
        // do wyświetlania (enable) yearComboBox oraz historyTextBox
        private void CheckYearBoxEnable()
        {
            if (OrdinaryBookRadio.Checked)
            {
                yearComboBox.Enabled = false;
                historyTextBox.Enabled = false;
            }
            if (LectureBookRadio.Checked)
            {
                yearComboBox.Enabled = true;
                historyTextBox.Enabled = false;
            }
            if (WhiteCrowRadio.Checked)
            {
                yearComboBox.Enabled = false;
                historyTextBox.Enabled = true;
            }
        }
        // Metoda sprawdzająca wartość - gatunek
        private string CheckGenreCombobox()
        {
            checkGenre = genreComboBox.Text;
            return checkGenre;
        }
        // Wprowadzenie danych z fomularza do zmiennych
        private void LoadVarFromForm()
        {
            // Tytuł: 
            titleSqlQuerry = titleTextBox.Text;
            // Autor: 
            authorSqlQuerry = authorTextBox.Text;
            // Gatunek: 
            genreSqlQuerry = CheckGenreCombobox();
            // Wydawnictwo/wydawca: 
            printSqlQuerry = printHouseTextBox.Text;
            // Typ: 
            typeSqlQuerry = GetTextFromRadioButtons();
            // Rok (do typu - lektura): 
            yearSqlQuerry = GetTextFromComboBox();
            // Opis: 
            descSqlQuerry = descTextBox.Text;
            // Historia (do - biały kruk): 
            historySqlQuerry = historyTextBox.Text;
        }
        // Metoda obsługująca zapytanie SQL (logika formularza) - 
        // sprawdzanie czy nie puste
        private void SqlQUeryHandling()
        {
            // Tytuł: 
            if (!string.IsNullOrWhiteSpace(titleTextBox.Text))
                titleSqlQuerry = titleTextBox.Text;
            else
            {
                titleSqlQuerry = "";
                errorBoxShow = (Environment.NewLine
                    + "Nie podano tytułu!"
                    + Environment.NewLine);
            }
            // Autor: 
            if (!string.IsNullOrWhiteSpace(authorTextBox.Text))
                authorSqlQuerry = authorTextBox.Text;
            else
            {
                authorSqlQuerry = "";
                errorBoxShow += (Environment.NewLine
                    + "Nie podano autora!"
                    + Environment.NewLine);
            }
            // Wydawnictwo/wydawca: 
            if (!string.IsNullOrWhiteSpace(printHouseTextBox.Text))
                printSqlQuerry = printHouseTextBox.Text;
            else
            {
                printSqlQuerry = "";
                errorBoxShow += (Environment.NewLine
                    + "Nie podano gatunku!"
                    + Environment.NewLine);
            }
            // Gatunek: 
            if (!string.IsNullOrWhiteSpace(genreComboBox.Text))
                genreSqlQuerry = genreComboBox.Text;
            else
            {
                genreSqlQuerry = "";
                errorBoxShow += (Environment.NewLine
                    + "Nie podano gatunku!"
                    + Environment.NewLine);
            }

            if (!string.IsNullOrWhiteSpace(errorBoxShow))
            MessageBox.Show(errorBoxShow 
                + Environment.NewLine
                + "Dane i tak zostana wprowadzone...", "BŁąd");
            errorBoxShow = null;
            
        }
        // Przycisk Dodaj pozycję
        private void AddBookButton_Click(object sender, EventArgs e)
        {
           LoadVarFromForm();
        //   SqlQUeryHandling();
            sqlQuerryInsert = "INSERT INTO Books "
                + "(Title, Author, Genre, 'Print House', Type, Year, Description, History) "
                + "VALUES ('" // Tytuł:
                + titleSqlQuerry
                + "','" // Autor: 
                + authorSqlQuerry
                + "','" // Gatunek: 
                + genreSqlQuerry
                + "','" // Wydawnictwo/wydawca: 
                + printSqlQuerry
                + "','" // Typ: 
                + typeSqlQuerry
                + "','" // Rok (do typu - lektura): 
                + yearSqlQuerry
                + "','" // Opis: 
                + descSqlQuerry
                + "','" // Historia (do - biały kruk): 
                + historySqlQuerry
                + "')";
            dbOperations.DBQuerry(sqlQuerryInsert);
        }
        // Typ książki
        private void OrdinaryBookRadio_CheckedChanged(object sender, EventArgs e)
        {
            CheckYearBoxEnable();
        }
        private void LectureBookRadio_CheckedChanged(object sender, EventArgs e)
        {
            CheckYearBoxEnable();
        }
        private void WhiteCrowRadio_CheckedChanged(object sender, EventArgs e)
        {
            CheckYearBoxEnable();
        }
    }
}
