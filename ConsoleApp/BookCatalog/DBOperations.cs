﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookCatalog
{
    public class DBOperations
    {
        public SQLiteConnection dbConnect;
        public DataSet dataSet;
        public SQLiteDataAdapter dataAdapter;
        public SQLiteCommand command, genreCommand;
        public SQLiteDataReader dataReader;
        public DataTable dataTable;
        private string DbPath { get; set; }
        private string Title { get; set; }
        private string Author { get; set; }
        private string Genre { get; set; }
        private string Print { get; set; }
        private string Type { get; set; }
        private int Year { get; set; }
        private string Description { get; set; }
        private string History { get; set; }
        public string selectedFolder = 
            @"C:\Users\mat3u\Documents\xopero\simple_BookCatalog\";
        public DBOperations(string dbPath)
        {
            DbPath = dbPath;
        }
        public DBOperations(string dbPath, string title, string author, string genre, string print, string type, int year, string description, string history)
        {
            DbPath = dbPath;
            Title = title;
            Author = author;
            Genre = genre;
            Print = print;
            Type = type;
            Year = year;
            Description = description;
            History = history;
        }
        // Połaczenie i otwarcie połączenia z baza danych
        public void DBConnection(string DbPath)
        {
            dbConnect = new SQLiteConnection("Data Source=" + DbPath + ";Version=3;");
            dbConnect.Open();
        }
        // Wprowadzanie danych do bazy
        public void DBQuerry(string sqlQuerry)
        {
            DBConnection(DbPath);
            command = dbConnect.CreateCommand();
            command.CommandText = sqlQuerry;
            command.ExecuteNonQuery();
            dbConnect.Close();
        }
    }
}
