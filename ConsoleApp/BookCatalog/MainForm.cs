﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace BookCatalog
{
    public partial class MainForm : Form
    {
        // Instancje fomularzy
        AddForm addForm;
        EditForm editForm;
        // Instancja klasy DBOperations
        DBOperations dbOperations;
        private string sqlQuerrySearch = "SELECT * FROM Books;", sqlQuerryDelete, sqlQuerryDeleteRow, titleSqlQuerry, authorSqlQuerry, genreSqlQuerry, printSqlQuerry;
        private bool checkBool;
        public MainForm()
        {
            InitializeComponent();
            dbOperations = new DBOperations(openFileDialog1.FileName);
        }
        // Wybór bazy danych
        public void DBSelectDataBase()
        {
            openFileDialog1.InitialDirectory = dbOperations.selectedFolder;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                dbOperations = new DBOperations(openFileDialog1.FileName);
        }
        // Wyświetlanie
        private void DBShow()
        {
            try
            {
                dbOperations.DBConnection(openFileDialog1.FileName);
                DBFillDataGridView();
            }
            catch (Exception)
            {
                throw;
            }
        }
        // Wyszukiwanie
        private void DBSelect()
        {
            checkBool = false;
            if (!checkBool)
            {
                // Tytuł: 
                if (string.IsNullOrWhiteSpace(titleTExtBox.Text))
                {
                    sqlQuerrySearch = "SELECT * FROM Books;";
                }
                else
                {
                    titleSqlQuerry = titleTExtBox.Text;
                    sqlQuerrySearch = "SELECT * "
                        + "FROM Books "
                        + "WHERE Title LIKE '"
                        + titleSqlQuerry
                        + "'";
                    checkBool = true;
                }
            }
            if (!checkBool)
            {
                // Autor: 
                if (string.IsNullOrWhiteSpace(authorTextBox.Text))
                {
                    sqlQuerrySearch = "SELECT * FROM Books;";
                }
                else
                {
                    authorSqlQuerry = authorTextBox.Text;
                    sqlQuerrySearch = "SELECT * "
                        + "FROM Books "
                        + "WHERE Author LIKE '"
                        + authorSqlQuerry
                        + "'";
                    checkBool = true;
                }
            }
            if (!checkBool)
            {
                // Wydawnictwo/wydawca: 
                if (string.IsNullOrWhiteSpace(printHouseTextBox.Text))
                {
                    sqlQuerrySearch = "SELECT * FROM Books;";
                }
                else
                {
                    printSqlQuerry = printHouseTextBox.Text;
                    sqlQuerrySearch = "SELECT * "
                        + "FROM Books "
                        + "WHERE Title LIKE '" 
                        + printSqlQuerry 
                        + "'";
                    checkBool = true;
                }
            }
            if (!checkBool)
            {
                // Gatunek: 
                if (string.IsNullOrWhiteSpace(genreComboBox.Text))
                {
                    sqlQuerrySearch = "SELECT * FROM Books;";
                }
                else
                {
                    genreSqlQuerry = genreComboBox.Text;
                    sqlQuerrySearch = "SELECT * "
                        + "FROM Books "
                        + "WHERE Title LIKE '"
                        + genreSqlQuerry
                        + "'";
                }
            }
        }
        // Wypełnianie dataGridView
        private void DBFillDataGridView()
        {
            dbOperations.dataSet = new DataSet();
            dbOperations.dataAdapter =
                new SQLiteDataAdapter(sqlQuerrySearch, dbOperations.dbConnect);
            dbOperations.dataAdapter.Fill(dbOperations.dataSet);
            dataGridView1.DataSource = dbOperations.dataSet.Tables[0].DefaultView;
        }
        // Wypełnianie genreComboBox
        /*
        private void DBFillGenreComboBox()
        {
            string sqlQuerryFillComboBox = "SELECT Genre FROM Books";
            dbOperations.genreCommand = new SQLiteCommand(sqlQuerryFillComboBox, dbOperations.dbConnect);
            SQLiteDataReader reader;
            reader = dbOperations.genreCommand.ExecuteReader();
            dbOperations.dataTable = new DataTable();
            dbOperations.dataTable.Columns.Add("Genre").ToString();
            dbOperations.dataTable.Load(reader);
            genreComboBox.DataSource = dbOperations.dataTable;
        }
        */
        // usuwanie wpisu z bazy danych
        private void DBDelete()
        {
            try
            {
                sqlQuerryDeleteRow =
                    dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                sqlQuerryDelete = "DELETE FROM Books WHERE ID='" + sqlQuerryDeleteRow + "'";
                dbOperations.DBQuerry(sqlQuerryDelete);
                DBShow();
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Zaznacz cały wiersz!", "Błąd");
            }
        }
        // Metoda włączając przyciski dopiero po wybraniu bazy danych
        private void ButtonEnabled()
        {
            SearchButton.Enabled = true;
            AddButton.Enabled = true;
            EditButton.Enabled = true;
            DeleteButton.Enabled = true;
        }
        // Przyciski
        // WYbór bazy danych (pliku)
        private void DataBaseSelect_Click(object sender, EventArgs e)
        {
            DBSelectDataBase();
            DBShow();
            // DBFillGenreComboBox();
            ButtonEnabled();
        }
        // Szukaj / odśwież
        private void SearchButton_Click(object sender, EventArgs e)
        {
            DBSelect();
            DBShow();
        }
        // Dodaj
        private void AddButton_Click(object sender, EventArgs e)
        {
            addForm = new AddForm(dbOperations);
            addForm.ShowDialog();
            DBShow();
        }
        // Edytuj
        private void EditButton_Click(object sender, EventArgs e)
        {
            editForm = new EditForm(dbOperations);
            editForm.ShowDialog();
            DBShow();
        }
        // usuń
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DBDelete();
            DBShow();
        }
    }
}
