﻿namespace BookCatalog
{
    partial class EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.genreComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.yearComboBox = new System.Windows.Forms.ComboBox();
            this.LectureBookRadio = new System.Windows.Forms.RadioButton();
            this.OrdinaryBookRadio = new System.Windows.Forms.RadioButton();
            this.EditBookButton = new System.Windows.Forms.Button();
            this.descTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.typeGroupBox = new System.Windows.Forms.GroupBox();
            this.WhiteCrowRadio = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.authorTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.printHouseTextBox = new System.Windows.Forms.TextBox();
            this.historyTextBox = new System.Windows.Forms.TextBox();
            this.historyLabel = new System.Windows.Forms.Label();
            this.typeGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // genreComboBox
            // 
            this.genreComboBox.FormattingEnabled = true;
            this.genreComboBox.Items.AddRange(new object[] {
            "Bajka ",
            "Bezpieczeństwo teleinformatyczne",
            "Dramat ",
            "Elegia ",
            "Fantastyka naukowa ",
            "Fikcja autobiograficzna ",
            "Nowela ",
            "Opowiadanie ",
            "Powieść ",
            "Powieść dystopijna ",
            "Powieść egzystencjalna ",
            "Powieść z kluczem \t",
            "Satyra ",
            "Literatura piękna",
            "Inżynieria społeczna"});
            this.genreComboBox.Location = new System.Drawing.Point(16, 227);
            this.genreComboBox.Name = "genreComboBox";
            this.genreComboBox.Size = new System.Drawing.Size(290, 32);
            this.genreComboBox.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(326, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 24);
            this.label6.TabIndex = 40;
            this.label6.Text = "Rok szkolny: ";
            // 
            // yearComboBox
            // 
            this.yearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.yearComboBox.Enabled = false;
            this.yearComboBox.FormattingEnabled = true;
            this.yearComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.yearComboBox.Location = new System.Drawing.Point(324, 227);
            this.yearComboBox.Name = "yearComboBox";
            this.yearComboBox.Size = new System.Drawing.Size(290, 32);
            this.yearComboBox.TabIndex = 39;
            // 
            // LectureBookRadio
            // 
            this.LectureBookRadio.AutoSize = true;
            this.LectureBookRadio.Location = new System.Drawing.Point(6, 65);
            this.LectureBookRadio.Name = "LectureBookRadio";
            this.LectureBookRadio.Size = new System.Drawing.Size(159, 28);
            this.LectureBookRadio.TabIndex = 1;
            this.LectureBookRadio.Text = "Lektura szkolna";
            this.LectureBookRadio.UseVisualStyleBackColor = true;
            this.LectureBookRadio.CheckedChanged += new System.EventHandler(this.LectureBookRadio_CheckedChanged);
            // 
            // OrdinaryBookRadio
            // 
            this.OrdinaryBookRadio.AutoSize = true;
            this.OrdinaryBookRadio.Checked = true;
            this.OrdinaryBookRadio.Location = new System.Drawing.Point(6, 31);
            this.OrdinaryBookRadio.Name = "OrdinaryBookRadio";
            this.OrdinaryBookRadio.Size = new System.Drawing.Size(179, 28);
            this.OrdinaryBookRadio.TabIndex = 0;
            this.OrdinaryBookRadio.TabStop = true;
            this.OrdinaryBookRadio.Text = "Ksiązka zwyczajna";
            this.OrdinaryBookRadio.UseVisualStyleBackColor = true;
            this.OrdinaryBookRadio.CheckedChanged += new System.EventHandler(this.OrdinaryBookRadio_CheckedChanged);
            // 
            // EditBookButton
            // 
            this.EditBookButton.Location = new System.Drawing.Point(16, 583);
            this.EditBookButton.Name = "EditBookButton";
            this.EditBookButton.Size = new System.Drawing.Size(290, 48);
            this.EditBookButton.TabIndex = 38;
            this.EditBookButton.Text = "Edytuj pozycję: ";
            this.EditBookButton.UseVisualStyleBackColor = true;
            this.EditBookButton.Click += new System.EventHandler(this.EditBookButton_Click);
            // 
            // descTextBox
            // 
            this.descTextBox.Location = new System.Drawing.Point(16, 300);
            this.descTextBox.Multiline = true;
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.Size = new System.Drawing.Size(598, 124);
            this.descTextBox.TabIndex = 37;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 273);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 24);
            this.label5.TabIndex = 36;
            this.label5.Text = "Opis:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 24);
            this.label4.TabIndex = 35;
            this.label4.Text = "Gatunek";
            // 
            // typeGroupBox
            // 
            this.typeGroupBox.Controls.Add(this.WhiteCrowRadio);
            this.typeGroupBox.Controls.Add(this.LectureBookRadio);
            this.typeGroupBox.Controls.Add(this.OrdinaryBookRadio);
            this.typeGroupBox.Location = new System.Drawing.Point(324, 9);
            this.typeGroupBox.Name = "typeGroupBox";
            this.typeGroupBox.Size = new System.Drawing.Size(290, 133);
            this.typeGroupBox.TabIndex = 34;
            this.typeGroupBox.TabStop = false;
            this.typeGroupBox.Text = "Typ:";
            // 
            // WhiteCrowRadio
            // 
            this.WhiteCrowRadio.AutoSize = true;
            this.WhiteCrowRadio.Location = new System.Drawing.Point(6, 99);
            this.WhiteCrowRadio.Name = "WhiteCrowRadio";
            this.WhiteCrowRadio.Size = new System.Drawing.Size(112, 28);
            this.WhiteCrowRadio.TabIndex = 2;
            this.WhiteCrowRadio.Text = "Biały kruk";
            this.WhiteCrowRadio.UseVisualStyleBackColor = true;
            this.WhiteCrowRadio.CheckedChanged += new System.EventHandler(this.WhiteCrowRadio_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 24);
            this.label3.TabIndex = 32;
            this.label3.Text = "Wydawnictwo:";
            // 
            // authorTextBox
            // 
            this.authorTextBox.Location = new System.Drawing.Point(16, 100);
            this.authorTextBox.Name = "authorTextBox";
            this.authorTextBox.Size = new System.Drawing.Size(290, 32);
            this.authorTextBox.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 24);
            this.label2.TabIndex = 30;
            this.label2.Text = "Autor:";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(16, 36);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(290, 32);
            this.titleTextBox.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 24);
            this.label1.TabIndex = 28;
            this.label1.Text = "Tytuł:";
            // 
            // printHouseTextBox
            // 
            this.printHouseTextBox.Location = new System.Drawing.Point(16, 165);
            this.printHouseTextBox.Name = "printHouseTextBox";
            this.printHouseTextBox.Size = new System.Drawing.Size(290, 32);
            this.printHouseTextBox.TabIndex = 33;
            // 
            // historyTextBox
            // 
            this.historyTextBox.Enabled = false;
            this.historyTextBox.Location = new System.Drawing.Point(16, 453);
            this.historyTextBox.Multiline = true;
            this.historyTextBox.Name = "historyTextBox";
            this.historyTextBox.Size = new System.Drawing.Size(598, 124);
            this.historyTextBox.TabIndex = 43;
            // 
            // historyLabel
            // 
            this.historyLabel.AutoSize = true;
            this.historyLabel.Location = new System.Drawing.Point(12, 426);
            this.historyLabel.Name = "historyLabel";
            this.historyLabel.Size = new System.Drawing.Size(85, 24);
            this.historyLabel.TabIndex = 42;
            this.historyLabel.Text = "Historia: ";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 643);
            this.Controls.Add(this.historyTextBox);
            this.Controls.Add(this.historyLabel);
            this.Controls.Add(this.genreComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.yearComboBox);
            this.Controls.Add(this.EditBookButton);
            this.Controls.Add(this.descTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.typeGroupBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.authorTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.printHouseTextBox);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditForm";
            this.ShowIcon = false;
            this.Text = "Edycja pozycji";
            this.typeGroupBox.ResumeLayout(false);
            this.typeGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox genreComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox yearComboBox;
        private System.Windows.Forms.RadioButton LectureBookRadio;
        private System.Windows.Forms.RadioButton OrdinaryBookRadio;
        private System.Windows.Forms.Button EditBookButton;
        private System.Windows.Forms.TextBox descTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox typeGroupBox;
        private System.Windows.Forms.RadioButton WhiteCrowRadio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox authorTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox printHouseTextBox;
        private System.Windows.Forms.TextBox historyTextBox;
        private System.Windows.Forms.Label historyLabel;
    }
}