﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookCatalog
{
    public partial class EditForm : Form
    {
        public SQLiteCommand command;
        DBOperations dbOperations;
        private string sqlQuerryEdit, titleSqlQuerry, authorSqlQuerry, genreSqlQuerry, printSqlQuerry, typeSqlQuerry, descSqlQuerry, historySqlQuerry, checkType, checkGenre, idSqlQuerry = "80";
        private int yearSqlQuerry, yearIntToNull;
        public EditForm(DBOperations dbOperations)
        {
            InitializeComponent();
            this.dbOperations = dbOperations;
         //   BDFillForm();
        }
        // Wypełnianie formularzy DODAJ / EDYTUJ danymi z wybranego rekordu
        private void BDFillForm()
        {

        }
        // Metoda zwracająca wartość - typ ksiazki
        private string GetTextFromRadioButtons()
        {
            if (OrdinaryBookRadio.Checked)
                checkType = "Ksiązka zwyczajna";
            if (LectureBookRadio.Checked)
                checkType = "Lektura";
            if (WhiteCrowRadio.Checked)
                checkType = "Biały kruk";
            return checkType;
        }


        // Metoda zwracająca wartość - rok szkolny
        private int GetTextFromComboBox()
        {
            if (yearComboBox.Text == "1")
                yearIntToNull = 1;
            if (yearComboBox.Text == "2")
                yearIntToNull = 2;
            if (yearComboBox.Text == "3")
                yearIntToNull = 3;
            return yearIntToNull;
        }
        // Metoda sprawdzająca radioButtony - typ ksiązki
        // do wyświetlania (enable) yearComboBox oraz historyTextBox
        private void CheckYearBoxEnable()
        {
            if (OrdinaryBookRadio.Checked)
            {
                yearComboBox.Enabled = false;
                historyTextBox.Enabled = false;
            }
            if (LectureBookRadio.Checked)
            {
                yearComboBox.Enabled = true;
                historyTextBox.Enabled = false;
            }
            if (WhiteCrowRadio.Checked)
            {
                yearComboBox.Enabled = false;
                historyTextBox.Enabled = true;
            }
        }
        // Metoda sprawdzająca wartość - gatunek
        private string CheckGenreCombobox()
        {
            checkGenre = genreComboBox.Text;
            return checkGenre;
        }
        // Wprowadzenie danych z fomularza do zmiennych
        private void LoadVarFromForm()
        {
            // Tytuł: 
            titleSqlQuerry = titleTextBox.Text;
            // Autor: 
            authorSqlQuerry = authorTextBox.Text;
            // Gatunek: 
            genreSqlQuerry = CheckGenreCombobox();
            // Wydawnictwo/wydawca: 
            printSqlQuerry = printHouseTextBox.Text;
            // Typ: 
            typeSqlQuerry = GetTextFromRadioButtons();
            // Rok (do typu - lektura): 
            yearSqlQuerry = GetTextFromComboBox();
            // Opis: 
            descSqlQuerry = descTextBox.Text;
            // Historia (do - biały kruk): 
            historySqlQuerry = historyTextBox.Text;
        }
        // Metoda obsługująca zapytanie SQL (logika formularza)
        private void SqlQUeryHandling()
        {

        }
        // Metoda pozwalająca na edytowanie elementów z bazy danych
        private void DBEdit()
        {
            LoadVarFromForm();
            sqlQuerryEdit = "UPDATE Books "
                + "SET Title = '" 
                + titleSqlQuerry 
                + "Author = '"
                + authorSqlQuerry + "''"
                /*
                + "' Author =" // Autor: 
                + authorSqlQuerry
                + "','Genre = " // Gatunek: 
                + genreSqlQuerry
                + "',' PrintHouse = " // Wydawnictwo/wydawca: 
                + printSqlQuerry
                + "',' Type = " // Typ: 
                + typeSqlQuerry
                + "',' Year = " // Rok (do typu - lektura): 
                + yearSqlQuerry
                + "','Descirption = " // Opis: 
                + descSqlQuerry
                + "','History = " // Historia (do - biały kruk): 
                + historySqlQuerry
                */
                + "WHERE ID = 80";
            dbOperations.DBQuerry(sqlQuerryEdit);
        }
        // Przycisk Edytuj pozycję
        private void EditBookButton_Click(object sender, EventArgs e)
        {
            DBEdit();
        }
        // Typ książki
        private void OrdinaryBookRadio_CheckedChanged(object sender, EventArgs e)
        {
            CheckYearBoxEnable();
        }
        private void LectureBookRadio_CheckedChanged(object sender, EventArgs e)
        {
            CheckYearBoxEnable();
        }
        private void WhiteCrowRadio_CheckedChanged(object sender, EventArgs e)
        {
            CheckYearBoxEnable();
        }
    }
}
