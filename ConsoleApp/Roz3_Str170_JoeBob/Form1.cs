﻿using System;
using System.Windows.Forms;

namespace Roz3_Str170_JoeBob
{
    public partial class Form1 : Form
    {
        Guy joe;
        Guy bob;
        int bank = 100;

        public Form1()
        {
            InitializeComponent();

            bob = new Guy
            {
                Name = "Bob",
                Cash = 100
            };
            joe = new Guy
            {
                Name = "Joe",
                Cash = 50
            };
            UpdateForm();

        }
        public void UpdateForm()
        {
            joesCashLabel.Text = joe.Name + " posiada  " + joe.Cash + " zł";
            bobsCashLabel.Text = bob.Name + " posiada " + bob.Cash + " zł";
            bankCashLabel.Text = "Bank posiada " + bank + " zł";
        }
        // ReceiveCash
        private void button1_Click(object sender, EventArgs e)
        {
            if (bank >= 10)
            {
                bank -= joe.ReceiveCash(10);
                UpdateForm();
            }
            else
            {
                MessageBox.Show("Bank nie posiada wystarczającej ilości pieniędzy");
            }
        }
        // GiveCash
        private void button2_Click(object sender, EventArgs e)
        {
            bank += bob.GiveCash(5);
            UpdateForm();
        }
        // Joe daje 10zł Bobowi
        private void joeGivesToBob_Click(object sender, EventArgs e)
        {
            joe.ReceiveCash(bob.GiveCash(10));
            UpdateForm();
        }
        // Bob daje 5zł Joemu
        private void bobGivesToJoe_Click(object sender, EventArgs e)
        {
            bob.ReceiveCash(joe.GiveCash(5));
            UpdateForm();
        }
        
    }
}
