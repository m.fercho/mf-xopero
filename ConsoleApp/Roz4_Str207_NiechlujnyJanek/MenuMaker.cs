﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz4_Str207_NiechlujnyJanek
{
    class MenuMaker
    {
        public Random Randomizeer;
        public string[] Meats = { "Pieczona wołowina", "Salami", "Indyk", "Szynka", "Karkówka" };
        public string[] Condiments = { "Musztarda żółta", "Musztrda brązowa", "Musztarda miodowa", "Majonez", "przyprawa", "Sos francuski" };
        public string[] Breads = { "Chleb ryżowy", "Chleb biały", "Chleb zozowy", "Pumpernikiel", " Cleb włoski", "Bułka" };

        public string GetMenuItem()
        {
            string randomMeat = Meats[Randomizeer.Next(Meats.Length)];
            string randomCondiments = Condiments[Randomizeer.Next(Condiments.Length)];
            string randomBread = Breads[Randomizeer.Next(Breads.Length)];
            return randomMeat + ", " + randomCondiments + ", " + randomBread;

        }
    }
}
