﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz8_Str414_Bird
{
    class Bird
    {
        public string Name { get; set; }
        public virtual void Fly()
        {
            Console.WriteLine("Frr... Frr...");
        }
        public override string ToString()
        {
            return "Ptak " + Name;
        }
    }
}
