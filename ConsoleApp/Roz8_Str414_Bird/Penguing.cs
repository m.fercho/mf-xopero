﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roz8_Str414_Bird
{
    class Penguing : Bird
    {
        public override void Fly()
        {
            Console.WriteLine("Pingwiny nie latają");
        }
        public override string ToString()
        {
            return "Pingwin " + Name;
        }
    }
}
