﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz7_Str368_Dom
{
    public partial class Form1 : Form
    {
        Location currentLocation;
        Outside garden;
        OutsideWithDoor frontYard, backYard;
        Room diningRoom;
        RoomWithDoor kitchen, livingRoom;

        public Form1()
        {
            InitializeComponent();
            CreateObject();
            MovieToANewLocation(livingRoom);
        }
        private void CreateObject()
        {
            garden = new Outside("ogród", false);
            frontYard = new OutsideWithDoor("Podwórko przed domem", false, "dębowe drzwi z mosiężną klamką");
            backYard = new OutsideWithDoor("Podwórko za domem", true, "rozsuwane drzwi");
            diningRoom = new Room("Jadalnia", "kryształowy żyrandol");
            livingRoom = new RoomWithDoor("Salon", "antyczny dywan", "dębowe drzwi z mosiężną klamką");
            kitchen = new RoomWithDoor("Kuchnia", "nierdzewne stalowe sztućce", "rozsuwane drzwi");

            garden.Exits = new Location[] { frontYard, backYard };
            frontYard.Exits = new Location[] { garden, livingRoom };
            backYard.Exits = new Location[] { garden, kitchen};
            diningRoom.Exits = new Location[] { livingRoom, kitchen};
            livingRoom.Exits = new Location[] { frontYard, diningRoom};
            kitchen.Exits = new Location[] { backYard, diningRoom};

            livingRoom.DoorLocation = frontYard;
            frontYard.DoorLocation = livingRoom;

            kitchen.DoorLocation = backYard;
            backYard.DoorLocation = kitchen;

        }
        private void MovieToANewLocation(Location newLocation)
        {
            currentLocation = newLocation;
            exits.Items.Clear();
            for (int i = 0; i < currentLocation.Exits.Length; i++)
            {
                exits.Items.Add(currentLocation.Exits[i].Name);
            }
            exits.SelectedIndex = 0;
            description.Text = currentLocation.Description;

            if (currentLocation is IHasExteriorDoor)
            {
                goTroughTheDoor.Enabled = true;
            }
            else
            {
                goTroughTheDoor.Enabled = false;
            }
        }
        private void goHere_Click(object sender, EventArgs e)
        {
            MovieToANewLocation(currentLocation.Exits[exits.SelectedIndex]);
        }

        private void goTroughTheDoor_Click(object sender, EventArgs e)
        {
            IHasExteriorDoor hasDoor = currentLocation as IHasExteriorDoor;
            MovieToANewLocation(hasDoor.DoorLocation);
        }
    }
}
