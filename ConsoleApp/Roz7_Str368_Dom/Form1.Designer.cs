﻿namespace Roz7_Str368_Dom
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.description = new System.Windows.Forms.TextBox();
            this.goHere = new System.Windows.Forms.Button();
            this.goTroughTheDoor = new System.Windows.Forms.Button();
            this.exits = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // description
            // 
            this.description.Location = new System.Drawing.Point(12, 12);
            this.description.Multiline = true;
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(358, 187);
            this.description.TabIndex = 0;
            // 
            // goHere
            // 
            this.goHere.Location = new System.Drawing.Point(12, 205);
            this.goHere.Name = "goHere";
            this.goHere.Size = new System.Drawing.Size(108, 23);
            this.goHere.TabIndex = 1;
            this.goHere.Text = "Idź tutaj";
            this.goHere.UseVisualStyleBackColor = true;
            this.goHere.Click += new System.EventHandler(this.goHere_Click);
            // 
            // goTroughTheDoor
            // 
            this.goTroughTheDoor.Enabled = false;
            this.goTroughTheDoor.Location = new System.Drawing.Point(12, 234);
            this.goTroughTheDoor.Name = "goTroughTheDoor";
            this.goTroughTheDoor.Size = new System.Drawing.Size(358, 48);
            this.goTroughTheDoor.TabIndex = 2;
            this.goTroughTheDoor.Text = "Przejdź przez drzwi";
            this.goTroughTheDoor.UseVisualStyleBackColor = true;
            this.goTroughTheDoor.Click += new System.EventHandler(this.goTroughTheDoor_Click);
            // 
            // exits
            // 
            this.exits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.exits.FormattingEnabled = true;
            this.exits.Location = new System.Drawing.Point(126, 204);
            this.exits.Name = "exits";
            this.exits.Size = new System.Drawing.Size(244, 24);
            this.exits.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 294);
            this.Controls.Add(this.exits);
            this.Controls.Add(this.goTroughTheDoor);
            this.Controls.Add(this.goHere);
            this.Controls.Add(this.description);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox description;
        private System.Windows.Forms.Button goHere;
        private System.Windows.Forms.Button goTroughTheDoor;
        private System.Windows.Forms.ComboBox exits;
    }
}

