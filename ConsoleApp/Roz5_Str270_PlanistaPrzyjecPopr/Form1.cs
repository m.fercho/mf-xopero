﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roz5_Str270_PlanistaPrzyjecPopr
{
    public partial class Form1 : Form
    {
        DinnerParty dinnerParty;
        public Form1()
        {
            InitializeComponent();
            dinnerParty = new DinnerParty((int)personNumeric.Value, healthyOption.Checked, fancyOption.Checked);
            DisplayDinnerPartyCost();
        }

        private void DisplayDinnerPartyCost()
        {
            decimal Cost = dinnerParty.Cost;
            resultLabel.Text = Cost.ToString("c");
        }

        private void personNumeric_ValueChanged(object sender, EventArgs e)
        {
            dinnerParty.NumberOfPeople = (int)personNumeric.Value;
            DisplayDinnerPartyCost();
        }

        private void fancyOption_CheckedChanged(object sender, EventArgs e)
        {
            dinnerParty.FancyDecorations = fancyOption.Checked;
            DisplayDinnerPartyCost();
        }

        private void healthyOption_CheckedChanged(object sender, EventArgs e)
        {
            dinnerParty.HealthyOption = healthyOption.Checked;
            DisplayDinnerPartyCost();
        }
    }
}
