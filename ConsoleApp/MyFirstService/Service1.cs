﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using MonitDLL;

namespace MyFirstService
{
    public partial class Service1 : ServiceBase
    {
        Monitor monitor;
        Timer timerToWrite = new Timer();
        private int HowOften;
        private string howOftenPath = @"C:\Users\mat3u\Documents\xopero\serviceSupportFolder\HowOftenFile.txt";
        public Service1()
        {
            InitializeComponent();
            monitor = new Monitor(HowOftenMeth());
        }
        // Instrukcje wykonywane po uruchomieniu usługi
        protected override void OnStart(string[] args)
        {
            WriteToFile(Environment.NewLine
                + "Usługa uruchomiona dnia: "
                + DateTime.Now.ToShortDateString()
                + " o godzinie: "
                + DateTime.Now.ToString("HH:mm:ss")
                + Environment.NewLine
                + "Zwracana wartość pobrana z pliku howOftenFile: "
                + HowOftenMeth()
                + ". ");
            timerToWrite.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timerToWrite.Interval = 20000; // 20 sekund
            timerToWrite.Enabled = true;
            monitor.MonitorStart();
        }
        // Instrukcje wykonywane po zatrzymaniu usługi
        protected override void OnStop()
        {
            WriteToFile(Environment.NewLine
                + "Usługa zatrzymana dnia: "
                + DateTime.Now.ToShortDateString()
                + " o godzinie: "
                + DateTime.Now.ToString("HH:mm:ss")
                + Environment.NewLine
                + "Zwracana wartość pobrana z pliku howOftenFile: "
                + HowOftenMeth()
                + ". ");
            monitor.MonitorStop();
        }
        // Instrukcje wykonywane w trakcie działania usługi
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            WriteToFile(Environment.NewLine
                + "Usługa wywołana dnia: "
                + DateTime.Now.ToShortDateString()
                + " o godzinie: "
                + DateTime.Now.ToString("HH:mm:ss")
                + Environment.NewLine
                + "Zwracana wartość pobrana z pliku howOftenFile: "
                + HowOftenMeth()
                + ". ");
            HowOftenMeth();
            monitor.TransferThing(HowOftenMeth());
        }
        // Odczytywanie danych z pliku 
        private int HowOftenMeth()
        {
            using (StreamReader sr = new StreamReader(howOftenPath))
            {
                HowOften = Convert.ToInt32(sr.ReadLine());
            }
            return HowOften;
        }
        // Zapis logów do pliku
        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory
                + "\\Logs\\ServiceLog_"
                + DateTime.Now.Date.ToShortDateString().Replace('/', '_')
                + ".txt";
            if (!File.Exists(filepath))
            {
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}  